local module       = {}
module["name"]     = "Droid Utilities"
module["kind"]     = "StaticLib"
module["src_root"] = DROID_ROOT .. "utilities/"
module["assets"]   = {}
module["project"]  = function(self)
	project ( self["name"] )
		kind ( self["kind"] )

		targetname "droid_utilities"

		targetdir( DROID_LIB_ROOT )

		files { self["src_root"] .. "**.h", self["src_root"] .. "**.cpp" }

		includedirs {
			self["src_root"],
			"lib/fbx/include"
		}

	copy_assets(self)
end

return module