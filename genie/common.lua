dlog = function(...)
	print("-> " .. ...)
end

dlog_ = function(...)
	io.write(...)
end

-- platform id
local host_def = "PLATFORM_UNSUPPORTED"
local host_os  = os.get()

if host_os == "windows" then
	host_def = "PLATFORM_WINDOWS"
elseif host_os == "linux" then
	host_def = "PLATFORM_LINUX"
elseif host_os == "macosx" then
	host_def = "PLATFORM_OSX"
end

local module       = {}
module["os"]       = host_os
module["os_def"]   = host_def
module["projects"] = {}
module["welcome"]  = function(self)
	-- hello
	dlog("")
	dlog("Droid Engine")
	dlog("------------")
	dlog("  Platform   : " .. self.os)
	dlog("  Root       : " .. DROID_ROOT)
	dlog("  Root.build : " .. DROID_BUILD_ROOT)
	dlog("  Root.lib   : " .. DROID_LIB_ROOT)
	dlog("  Root.bin   : " .. DROID_BIN_ROOT)

	if _OPTIONS["build-test"] then
		dlog("  Build Test : YES")
	end

	dlog("------------")
	dlog("")
end

module["copyfile"] = function(self,from_path,to_path)

	if os.isfile(from_path) == nil then
		return nil,from_path .. " not found"
	end

	-- extract all relative paths 
	local dirs = string.explode( path.getdirectory(to_path), "/")

	-- create intermediate directories 
	local lastdir = DROID_ROOT
	for k,v in pairs(dirs) do

		local nextdir = lastdir .. v

		if ( os.isdir(nextdir) == false ) then
			local res,err = os.mkdir(nextdir)

			if ( res == nil ) then
				return nil, err
			end
		end 

		lastdir = nextdir .. "/"
	end

	-- if nothing failed, copy the file
	os.copyfile(from_path,to_path)

	return true, nil
end

module["add_project"] = function(self,proj,kind)
	dlog("Adding project " .. proj["name"])
	if kind ~= nil then
		proj["kind"] = kind
	end

	module["projects"][proj["name"]] = proj
end

module["generate"] = function(self)
	for k,v in pairs(self["projects"]) do
		dlog("Generating project " .. v["name"])
		v["project"](v)
	end
end

return module