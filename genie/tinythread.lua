local module       = {}
module["name"]     = "TinyThread"
module["kind"]     = "StaticLib"
module["src_root"] = DROID_ROOT .. "lib/tinythread"
module["project"]  = function(self)
	project ( self["name"] )
		kind ( self["kind"] )

		targetname "TinyThread"

		targetdir( DROID_LIB_ROOT )

		files { self["src_root"] .. "**.h", self["src_root"] .. "**.cpp" }

		defines {
			_defines
		}

		includedirs {
			self["src_root"]
		}

		configuration {"windows"}
			defines { "_ITERATOR_DEBUG_LEVEL=0" }

end

return module