local platformer = require("examples/platformer/project")

local module       = {}
module["name"]     = "Droid Examples"
module["projects"] = { platformer }
module["project"]  = function(self)
	for k,v in pairs(self["projects"]) do
		v:project()
	end
end

return module