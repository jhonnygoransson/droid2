local module       = {}
module["name"]     = "Droid Engine"
module["kind"]     = "StaticLib"
module["src_root"] = DROID_ROOT .. "src/"
module["assets"]   = {}
module["project"]  = function(self)
	dlog("Attaching " .. self["name"])

	project ( self["name"] )
		kind ( self["kind"] )

		targetname "droid_static"

		targetdir( DROID_LIB_ROOT )

		files { self["src_root"] .. "**.h", 
				self["src_root"] .. "**.cpp",
				self["src_root"] .. "**.inl" }

		defines {
			_defines,
			"DROID_PROFILING"
		}

		includedirs {
			self["src_root"],
			self["src_root"] .. "core",
			self["src_root"] .. "core/file_system",
			"lib/tinythread/",
			"lib/lua/",
			"lib/brofiler/",
			"lib/bgfx/include/",
			"lib/bx/include/",
			"lib/linmath.h/"
		}

		-- workaround for bgfx with vs2008 compilers
		-- __STDC_LIMIT_MACROS is needed to pull in defines from c99-stdint that bgfx needs
		configuration { "vs2008" }
			includedirs {"lib/bgfx/include/compat/"}
			defines     {"__STDC_LIMIT_MACROS", "__STDC_CONSTANT_MACROS"}

		configuration { "windows" }
			includedirs { self["src_root"] .. "graphics/win32/"}
			defines { "_ITERATOR_DEBUG_LEVEL=0" }

		configuration { "windows", "x32" }
			libdirs {
				DROID_ROOT .. "lib/brofiler/x86/",
				DROID_ROOT .. "lib/lua/x86/"
			}

			links { "ProfilerCore32" }
			postbuildcommands { "echo d | xcopy /C /E /I /y ..\\..\\..\\lib\\brofiler\\x86\\ProfilerCore32.dll ..\\bin\\" }

		configuration { "windows", "x64" }
			libdirs( DROID_ROOT .. "lib/brofiler/x64/" )
			links { "ProfilerCore64" }

			postbuildcommands { "echo d | xcopy /C /E /I /y ..\\..\\..\\lib\\brofiler\\x64\\ProfilerCore64.dll ..\\bin\\" }

		configuration "Debug"
			defines { "DROID_DEBUG" }

		configuration{"not windows"}
			excludes { 
				self["src_root"] .. "core/file_system/windows/*.*",
				self["src_root"] .. "util/windows/*.*",
				self["src_root"] .. "graphics/win32/*.*"
			}

	copy_assets(self)
end

return module