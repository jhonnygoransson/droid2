-- paths
DROID_ROOT       = (path.getabsolute("") .. "/")
DROID_BUILD_ROOT = DROID_ROOT .. "build/" .. _ACTION
DROID_PROJ_ROOT  = DROID_BUILD_ROOT .. "/projects/"
DROID_BIN_ROOT   = DROID_BUILD_ROOT .. "/bin/"
DROID_LIB_ROOT   = DROID_BUILD_ROOT .. "/lib/"

-- modules
local common     = require("genie/common")
local droid      = require("genie/droid")
local tinythread = require("genie/tinythread")
local utilities  = require("genie/utilities")
local examples   = require("genie/examples")
local test       = require("test/test")

function copy_assets(module)

	dlog("Copying assets for module " .. module.name)

	if ( module.assets ) then
		for k,v in pairs(module.assets) do

			local from_path = nil
			local to_path = nil

			if ( type(v) == "table" ) then
				from_path = v[1]
				to_path   = v[2]
			else
				from_path = v
				to_path   = v
			end

			dlog_("->  " .. from_path .. " -> " .. to_path)

			to_path       = path.join(DROID_PROJ_ROOT, to_path)
			to_path       = string.gsub(to_path,DROID_ROOT,"")

			local res,err = common:copyfile(from_path,to_path)

			if res == nil then
				dlog_(" FAILED : " .. err)
			else 
				dlog_(" SUCCESS")
			end

			dlog_("\n")
		end
	end
end

-- options
newoption {
	trigger = "build-test",
	description = "Build test project located under /test"
}

newoption {
	trigger = "build-examples",
	description = "Build example projects located under /examples"
}

-- show welcome msg
common:welcome()

-- main solution / project
solution "Droid"
	configurations { "Debug", "Release" }
	platforms { "x32", "x64" }
	language "C++"

	flags { "FatalWarnings", "NoPCH" }

	configuration "Debug"
		defines { "DEBUG", host_def }
		flags { "Symbols" }

	configuration "Release"
		defines { "NDEBUG", host_def }
		flags { "Optimize" }

	includedirs { }
	location( DROID_PROJ_ROOT )
	startproject "studio"

common:add_project(droid)
common:add_project(utilities)
common:add_project(tinythread)

if _OPTIONS["build-test"] then
	common:add_project(test)
end

if _OPTIONS["build-examples"] then
	common:add_project(examples)
end

common:generate()