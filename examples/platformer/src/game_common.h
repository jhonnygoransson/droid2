#pragma once

#include "core/droid_common.h"
#include "math/droid_math.h"
#include "core/droid_logger.h"
#include "graphics/droid_graphics_handles.h"

namespace Droid
{
	class PlatformContext;
	class Engine;
	class GraphicsSystem;
	class GameComponentSystem;
	class Configurator;
	class VertexDeclaration;
	struct VertexBufferHandle;
	struct IndexBufferHandle;
}

struct EngineState
{	
	Droid::PlatformContext*     ctx;
	Droid::Engine*              engine;
	Droid::GraphicsSystem*      gfx;
	Droid::GameComponentSystem* gcs;
	Droid::Configurator*        config;
	Droid::Logger*              logger;
};

enum QBatchCmd
{
	Begin,
	Add,
	Edit,
	End
};

struct QBatchVertex
{
	float vx;
	float vy;
	float vz;
	unsigned char  r;
	unsigned char  g;
	unsigned char  b;
	unsigned char  a;
};

typedef unsigned short int IndexData;

struct QBatchDesc
{
	QBatchDesc()
	: vx_decl(0)
	, vb_handle(0)
	, ib_handle(0)
	{}

	Droid::VertexDeclaration*   vx_decl;
	Droid::VertexBufferHandle*  vb_handle;
	Droid::IndexBufferHandle*   ib_handle;
	Droid::Array<QBatchVertex>  vx_data;
	Droid::Array<IndexData>     ix_data;
};

typedef unsigned int QBatchHandle;

struct MaterialDesc 
{
	MaterialDesc() 
	: programHandle(0)
	{}

	Droid::ProgramHandle* programHandle;
};

typedef unsigned int MaterialHandle;

struct RenderOp
{
	QBatchHandle   meshId;
	MaterialHandle materialId;

	Droid::Mat4x4 viewMatrix;
	Droid::Mat4x4 modelMatrix;
	Droid::Mat4x4 projectionMatrix;

};

typedef Droid::Array<RenderOp> RenderOperations;