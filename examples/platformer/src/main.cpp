#include "game.h"

int main(int argc, char const *argv[])
{
	Game         game;
	GamePlay     gamePlay;
	GameRenderer gameRenderer;
	
	game.Attach(&gamePlay);
	game.Attach(&gameRenderer); 

	return game.Run();
}