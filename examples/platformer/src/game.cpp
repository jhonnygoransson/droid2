#include <assert.h>

#include "game.h"

#include "droid.h"

#include "core/droid_engine.h"
#include "core/droid_profiling.h"
#include "core/droid_configurator.h"

#include "graphics/droid_graphics_system.h"

#include "game/component/droid_game_component_system.h"

class Game::Impl
{
public:
	Impl()
	: gamePlay(0) 
	{}

	int Run() 
	{ 
		assert(gamePlay);

		Droid::GraphicsSystem* gfx = Droid::GraphicsSystem::Create();
		Droid::GameComponentSystem* gcs = Droid::GameComponentSystem::Create();
		Droid::PlatformContext* cfg = new Droid::PlatformContext;

		state.engine = Droid::Engine::Create( *(cfg) );
		state.engine->Attach(gfx);
		state.engine->Attach(gcs);
		state.engine->Initialize();

		state.ctx    = cfg;
		state.gfx    = gfx;
		state.gcs    = gcs;
		state.config = static_cast<Droid::Configurator*>(state.engine->GetSystem("Configurator"));
		state.logger = static_cast<Droid::Logger*>(state.engine->GetSystem("Logger"));

		gameRenderer->Init();
		gamePlay->Init();

		while( state.engine->GetEngineState() == Droid::Running )
		{ 
			DROID_PF_FRAME("MAIN")

			gamePlay->Update();

			state.engine->PumpMessages();
			state.engine->Update(0.0);
		}

		Droid::Engine::Destroy();

		return 0;
	}

	void Attach(GamePlay* gamePlay)
	{
		this->gamePlay = gamePlay;
	}

	void Attach(GameRenderer* gameRenderer)
	{
		this->gameRenderer = gameRenderer;
	}

	EngineState& GetState()
	{
		return state;
	}

	GameRenderer& GetGameRenderer()
	{
		return *gameRenderer;
	}
	GamePlay& GetGamePlay()
	{
		return *gamePlay;
	}

	EngineState   state;
	GamePlay*     gamePlay;
	GameRenderer* gameRenderer;
};

Game::Game() : impl(new Impl)
{}

Game::~Game()
{
	delete impl;
}

int Game::Run()
{
	return impl->Run();
}

void Game::Attach(GamePlay* gamePlay)
{
	impl->Attach(gamePlay);
	gamePlay->SetGame(this);
}

void Game::Attach(GameRenderer* gameRenderer)
{
	impl->Attach(gameRenderer);
	gameRenderer->SetGame(this);
}

EngineState& Game::GetState()
{
	return impl->GetState();
}

GameRenderer& Game::GetGameRenderer()
{
	return impl->GetGameRenderer();
}

GamePlay& Game::GetGamePlay()
{
	return impl->GetGamePlay();
}