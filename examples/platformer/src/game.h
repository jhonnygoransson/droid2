#pragma once

#include "game_common.h"

class Game;
class GamePlay;
class GameRenderer;

class GameRenderer
{
	class Impl;
	Impl* impl;
public:
	GameRenderer();
	~GameRenderer();

	void SetGame(Game* game);
	void Init();
	QBatchHandle Batch(QBatchCmd cmd);
	MaterialHandle Material(const char* vsPath, const char* fsPath);
};

class GamePlay
{
	class Impl;
	Impl* impl;
public:
	GamePlay();
	~GamePlay();

	void SetGame(Game* game);
	void Init();
	void Update();
	void Shutdown();
	RenderOperations& GetRenderOperations();
};

class Game
{
	class Impl;
	Impl* impl;
public:
	Game();
	~Game();
	int Run();
	void Attach(GamePlay* gamePlay);
	void Attach(GameRenderer* gameRenderer);
	EngineState& GetState();
	GameRenderer& GetGameRenderer();
	GamePlay& GetGamePlay();
};