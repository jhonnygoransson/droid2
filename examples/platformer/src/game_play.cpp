#include "game.h"
#include "game/component/droid_game_component.h"
#include "game/component/droid_game_component_system.h"

enum ObjectType
{
	Wall,
	Ice,
	Player,
	Etc
};

struct block_render_component : public Droid::GameComponent
{
	QBatchHandle   batchHandle;
	MaterialHandle materialHandle;
};

struct block_component : public Droid::GameComponent
{
	int x;
	int y;
	int w;
	int h;
};

#define cell_not_found -1

struct cell_t
{	
	Droid::Array<Droid::GameEntity::GuidType> entities;
	unsigned int x;
	unsigned int y;
};

struct grid_t
{
	int                  cells_x;
	int                  cells_y;
	Droid::Array<cell_t> cells;

	inline void resize(int x, int y)
	{
		cells_x = x;
		cells_y = y;

		cells.Resize(x*y);

		for(unsigned int cy=0; cy < static_cast<unsigned int>(y); cy++)
		for(unsigned int cx=0; cx < static_cast<unsigned int>(x); cx++)
		{
			int index = cy * y + cx;
			cells[index].x = cx;
			cells[index].y = cy;
		}
	}

	inline cell_t& get_cell(unsigned int x, unsigned int y)
	{
		assert(x*y < cells.Size());

		unsigned int idx = y * cells_x + x;
		return cells[idx];
	}

	// pushes an entity to a cell, doesn't check if already exists
	inline cell_t& put(Droid::GameEntity& entity, unsigned int x, unsigned int y)
	{
		assert(x*y < cells.Size());

		cell_t& cell = get_cell(x,y);
		Droid::GameEntity::GuidType guid = entity.GetId();
		cell.entities.Push(guid);

		return cell;
	}

	inline void put_unique(Droid::GameEntity& entity, unsigned int x, unsigned int y)
	{
		assert(x*y < cells.Size());

		if (find_first_in_cell(entity,x,y) == cell_not_found)
		{
			put(entity,x,y);
		}
	}

	// return index in cell array if found, -1 otherwise
	inline int find_first_in_cell(Droid::GameEntity& entity, unsigned int x, unsigned int y)
	{
		assert(x*y < cells.Size());

		cell_t& cell = get_cell(x,y);
		for(unsigned int i; i < cell.entities.Size(); i++)
		{
			if (cell.entities[i] == entity.GetId())
				return i;
		}

		return cell_not_found;
	}

	inline void remove(Droid::GameEntity& entity, unsigned int x, unsigned int y)
	{
		assert(x*y < cells.Size());

		int index_in_cell = find_first_in_cell(entity,x,y);

		// what to do here
	}
};

class GamePlay::Impl
{
public:
	Droid::GameEntity& ObjectFactory(ObjectType objType)
	{
		EngineState& state = game->GetState();
		switch(objType)
		{
			case(Wall):
			{
				Droid::GameEntity& newWallEntity = state.gcs->NewEntity();
				assert(newWallEntity != Droid::GameEntity::NONE);

				newWallEntity.AddComponent(blockComponentType);
				newWallEntity.AddComponent(blockComponentRenderType);

				block_render_component* renderComponent = state.gcs->GetComponent<block_render_component>(newWallEntity);
				block_component* blockComponent = state.gcs->GetComponent<block_component>(newWallEntity);

				renderComponent->batchHandle    = staticBlockBatchHandle;
				renderComponent->materialHandle = staticTestMaterial;

				blockComponent->x    = 0;
				blockComponent->y    = 0;
				blockComponent->w    = blockDimensions.width;
				blockComponent->h    = blockDimensions.height;

				return newWallEntity;
			}
		}

		return Droid::GameEntity::NONE;
	}

	void PrintGrid()
	{
		EngineState& state = game->GetState();

		for(int cy=0; cy < grid.cells_y; cy++)
		for(int cx=0; cx < grid.cells_x; cx++)
		{
			int index = cy * grid.cells_y + cx;

			if (grid.cells[index].entities.Size() > 0)
			{
				state.logger->Log(DROID_LOG_INFO,"GamePlay","Cell [%i,%i]",cx,cy);
				state.logger->Log(DROID_LOG_INFO,"GamePlay","============");

				for(unsigned int e=0;e < grid.cells[index].entities.Size(); e++)
				{
					Droid::GameEntity& ent = state.gcs->GetEntityByGuid(grid.cells[index].entities[e]);
					block_component* blockComponent = state.gcs->GetComponent<block_component>(ent);
					state.logger->Log(DROID_LOG_INFO,"GamePlay","  %u (%i,%i) @ %p",
						grid.cells[index].entities[e],
						blockComponent->x,
						blockComponent->y,
						(void*)blockComponent);
				}
			}
		}
	}

	void PlaceBlockInGridCell( Droid::GameEntity& entity, unsigned int cx, unsigned int cy )
	{
		assert(entity.IsTaken() && entity.HasComponent(blockComponentType));

		EngineState& state = game->GetState();
		cell_t& cell       = grid.put(entity,cx,cy);

		block_component* blockComponent = state.gcs->GetComponent<block_component>(entity);

		int cell_step_w = playArea.width / blockDimensions.width;
		int cell_step_h = playArea.height / blockDimensions.height;

		blockComponent->x = cell.x;
		blockComponent->y = cell.y;
	}

	void MakeLevel()
	{
		Droid::GameEntity& wall0 = ObjectFactory(Wall);
		Droid::GameEntity& wall1 = ObjectFactory(Wall);
		Droid::GameEntity& wall2 = ObjectFactory(Wall);
		Droid::GameEntity& wall3 = ObjectFactory(Wall);

		PlaceBlockInGridCell(wall0,0,0);
		PlaceBlockInGridCell(wall1,1,0);

		PlaceBlockInGridCell(wall2,2,0);
		PlaceBlockInGridCell(wall3,3,0);

		PrintGrid();
	}

	void Init()
	{
		EngineState& state = game->GetState();

		// grid
		grid.resize(8,8);

		GameRenderer& gr = game->GetGameRenderer();

		// render batches
		staticBlockBatchHandle = gr.Batch(Begin);
		gr.Batch(Add);
		gr.Batch(End);

		// materials
		staticTestMaterial = gr.Material("E:/dev/droid2/test/vs_cubes.bin","E:/dev/droid2/test/fs_cubes.bin");

		// entities 
		state.gcs->AllocateEntities(8*8);
		blockComponentType       = state.gcs->AllocateComponent<block_component>();
		blockComponentRenderType = state.gcs->AllocateComponent<block_render_component>();

		playArea.width  = 128;
		playArea.height = 128;

		blockDimensions.width  = playArea.width / grid.cells_x;
		blockDimensions.height = playArea.height / grid.cells_y;

		MakeLevel();
	}

	RenderOperations& GetRenderOperations()
	{
		return renderOps;
	}

	void Update()
	{
		EngineState& state = game->GetState();

		renderOps.Reset();
		

		Droid::Mat4x4 projectionMatrix;
		Droid::Mat4x4Ortho(projectionMatrix, 
			0.0f, static_cast<float>(playArea.width),
			static_cast<float>(playArea.height), 0.0f, 
			-1.0f, 1.0f);

		Droid::Mat4x4 viewMatrix;
		Droid::Mat4x4Identity(viewMatrix);

		for(unsigned int i=0;i<state.gcs->GetCapacity();i++)
		{
			Droid::GameEntity& ent = state.gcs->GetEntity(i);

			if (ent.IsTaken())
			{
				if (ent.HasComponent(blockComponentRenderType))
				{
					block_render_component* renderComponent = state.gcs->GetComponent<block_render_component>(ent);

					RenderOp op;
					op.meshId     = renderComponent->batchHandle;
					op.materialId = renderComponent->materialHandle;

					Droid::Mat4x4Identity(op.modelMatrix);

					if (ent.HasComponent(blockComponentType))
					{
						block_component* blockComponent = state.gcs->GetComponent<block_component>(ent);

						// compensate for quadbatches adding block in center
						// grid positions are positioned at top left 
						float rx = static_cast<float>(blockComponent->x) + 0.5f;
						float ry = static_cast<float>(blockComponent->y) + 0.5f;

						Droid::Mat4x4 translateMatrix;
						Droid::Mat4x4Translate(translateMatrix, rx, ry,0.0f);
	
						Droid::Mat4x4 scaleMatrix;
						Droid::Mat4x4Identity(scaleMatrix);

						scaleMatrix[0][0] = static_cast<float>(blockComponent->w);
						scaleMatrix[1][1] = static_cast<float>(blockComponent->h);
						scaleMatrix[2][2] = 1.0f;
						scaleMatrix[3][3] = 1.0f;

						Droid::Mat4x4Mul(op.modelMatrix,scaleMatrix,translateMatrix);
					}

					memcpy( (void*) op.viewMatrix, (void*) viewMatrix, sizeof(Droid::Mat4x4) );
					memcpy( (void*) op.projectionMatrix, (void*) projectionMatrix, sizeof(Droid::Mat4x4) );

					renderOps.Push(op);
				}
			}
		}
	}

	void Shutdown()
	{
		this->game = 0;
	}

	// member vars
	Game*  game;
	grid_t grid;

	Droid::GameComponentType blockComponentType;
	Droid::GameComponentType blockComponentRenderType;

	QBatchHandle staticBlockBatchHandle;
	MaterialHandle staticTestMaterial;
	RenderOperations renderOps;

	struct
	{
		int width;
		int height;
	} playArea;

	struct
	{
		int width;
		int height;
	} blockDimensions;
};

GamePlay::GamePlay() : impl(new Impl)
{

}

GamePlay::~GamePlay()
{
	delete impl;
}

void GamePlay::Init()
{
	impl->Init();
}

void GamePlay::Update()
{
	impl->Update();
}

void GamePlay::Shutdown()
{
	impl->Shutdown();
}

void GamePlay::SetGame(Game* game)
{
	impl->game = game;
}

RenderOperations& GamePlay::GetRenderOperations()
{
	return impl->GetRenderOperations();
}