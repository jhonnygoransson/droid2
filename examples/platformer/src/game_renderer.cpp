#include "game.h"

#include "droid.h"
#include "core/droid_common.h"
#include "core/droid_engine.h"
#include "core/droid_event.h"
#include "core/droid_configurator.h"
#include "core/file_system/droid_file_disk.h"

#include "graphics/droid_graphics_system.h"
#include "graphics/droid_viewport.h"

class GameRenderer::Impl : public Droid::Subscriber, Droid::ViewportRenderer
{
public:
	Impl(){}
	~Impl(){}

	void Init()
	{
		EngineState& state = game->GetState();

		viewport = state.gfx->CreateViewport();
		viewport->SetRect(Droid::WindowRect());
		viewport->SetViewportRenderer(this);

		state.engine->AddSubscriber(this);
		state.gfx->AddSubscriber(this);

		lastBatchcmd = End;
	}

	void Render(Droid::Viewport* vp, double dt)
	{
		EngineState& state          = game->GetState();
		RenderOperations& renderOps = game->GetGamePlay().GetRenderOperations();
		Droid::GraphicsSystem* gfx  = state.gfx;

		for (unsigned int i=0; i < renderOps.Size(); i++)
		{
			RenderOp op       = renderOps[i];
			QBatchDesc& batch = batches[op.meshId];
			MaterialDesc& mat = materials[op.materialId];

			gfx->SetViewTransform(vp->GetId(), op.viewMatrix, op.projectionMatrix);
			gfx->SetModelTransform(op.modelMatrix);

			gfx->SetVertexBuffer(batch.vb_handle);
			gfx->SetIndexBuffer(batch.ib_handle);

			gfx->SetState(Droid::RenderState::Default);

			gfx->Submit(vp->GetId(),mat.programHandle);
		}
	}

	void OnEvent(const Droid::Event evt)
	{
		if (evt == Droid::WindowEvents::Resize )
		{
			EngineState& state = game->GetState();

			int width  = state.config->GetConfigAsInt("WindowWidth");
			int height = state.config->GetConfigAsInt("WindowHeight");

			viewport->SetRect(Droid::WindowRect(0,0,width,height));
		}
	}

	MaterialHandle Material(const char* vsPath, const char* fsPath)
	{
		EngineState& state = game->GetState();

		Droid::FileDisk fileDisk;
		Droid::Path _vsPath(vsPath);
		Droid::Path _fsPath(fsPath);

		fileDisk.Open(_vsPath, Droid::FileSystem::READ);
		Droid::ShaderHandle* vsHandle = state.gfx->CreateShader(fileDisk);
		fileDisk.Close();

		fileDisk.Open(_fsPath, Droid::FileSystem::READ);
		Droid::ShaderHandle* fsHandle = state.gfx->CreateShader(fileDisk);
		fileDisk.Close();

		Droid::ProgramHandle* programHandle = state.gfx->CreateProgram(vsHandle,fsHandle);
		MaterialHandle       materialHandle = materials.Size();

		MaterialDesc newMaterial;
		newMaterial.programHandle = programHandle;
	
		materials.Push(newMaterial);

		return materialHandle;
	}

	QBatchHandle Batch(QBatchCmd cmd)
	{
		QBatchHandle handle = batches.Size();


		switch(cmd)
		{
			case(Begin):
			{
				assert(lastBatchcmd == End);
				QBatchDesc newBatch;

				newBatch.vx_decl = Droid::VertexDeclaration::Create();
				newBatch.vx_decl->AddAttribute( Droid::Attribute::Position, 3, Droid::AttributeType::Float);
				newBatch.vx_decl->AddAttribute( Droid::Attribute::Color0, 4, Droid::AttributeType::UInt8,true);
				newBatch.vx_decl->Commit();

				batches.Push(newBatch);
			} break;
			case(Add):
			{
				assert(lastBatchcmd != End);

				QBatchDesc& batch   = batches[batches.Size()-1];
				QBatchVertex vx0 = { 0.5f,  0.5f, 0.0f, 0xFF,0xFF,0,0xFF};
				QBatchVertex vx1 = {-0.5f, -0.5f, 0.0f, 0,0,0,0xFF};
				QBatchVertex vx2 = { 0.5f, -0.5f, 0.0f, 0xFF,0,0,0xFF};
				QBatchVertex vx3 = {-0.5f,  0.5f, 0.0f, 0,0xFF,0,0xFF};

				batch.vx_data.Push(vx0);
				batch.vx_data.Push(vx1);
				batch.vx_data.Push(vx2);
				batch.vx_data.Push(vx3);

				batch.ix_data.Push(0);
				batch.ix_data.Push(2);
				batch.ix_data.Push(1);
				batch.ix_data.Push(0);
				batch.ix_data.Push(1);
				batch.ix_data.Push(3);

			} break;
			case(Edit):
			{
				// not implemented
				assert(0); 
			} break;
			case(End):
			{
				assert(lastBatchcmd == Add);

				EngineState& state = game->GetState();
				QBatchDesc& batch      = batches[batches.Size()-1];

				char* vertexData   = reinterpret_cast<char*>(&batch.vx_data[0]);
				char* indexData    = reinterpret_cast<char*>(&batch.ix_data[0]);

				{
					assert(batch.vb_handle == 0);

					Droid::FileMemory vxDataFile;
					size_t            vxDataFileSize = sizeof(QBatchVertex) * batch.vx_data.Size();

					vxDataFile.Open(Droid::FileSystem::WRITE | Droid::FileSystem::READ);
					vxDataFile.Write(vertexData, vxDataFileSize);
					batch.vb_handle = state.gfx->CreateVertexBuffer(batch.vx_decl, vxDataFile);
					vxDataFile.Close();
				}

				{
					assert(batch.ib_handle == 0);

					Droid::FileMemory ixDataFile;
					size_t            ixDataFileSize = sizeof(IndexData) * batch.ix_data.Size();

					ixDataFile.Open(Droid::FileSystem::WRITE | Droid::FileSystem::READ);
					ixDataFile.Write(indexData, ixDataFileSize);
					batch.ib_handle = state.gfx->CreateIndexBuffer(ixDataFile);
					ixDataFile.Close();
				}

			} break;
		}

		lastBatchcmd = cmd;

		return handle;
	}

	Game*                      game;
	Droid::Viewport*           viewport;
	Droid::Array<QBatchDesc>       batches;
	Droid::Array<MaterialDesc> materials;
	QBatchCmd                  lastBatchcmd;
};

GameRenderer::GameRenderer()
: impl(new Impl)
{

}

GameRenderer::~GameRenderer()
{
	delete impl;
}

void GameRenderer::Init()
{
	impl->Init();
}

void GameRenderer::SetGame(Game* game)
{
	impl->game = game;
}

QBatchHandle GameRenderer::Batch(QBatchCmd cmd)
{
	return impl->Batch(cmd);
}

MaterialHandle GameRenderer::Material(const char* vsPath, const char* fsPath)
{
	return impl->Material(vsPath,fsPath);
}