local module      = {}

module["name"]     = "Droid Platformer Example"
module["kind"]     = "ConsoleApp"
module["src_root"] = "examples/platformer/"
module["assets"]   = 
{ 
	-- { "test/api_test.lua", "data/scripts/api_test.lua" }
}
module["project"]  = function(self)
	dlog("Attaching " .. self["name"])

	project ( self["name"] )
		kind ( self["kind"] )

		targetdir( DROID_BIN_ROOT )

		files { self["src_root"] .. "**.h", self["src_root"] .. "**.cpp" }

		defines {
			_defines,
			"DROID_PROFILING"
		}

		includedirs {
			self["src_root"],
			"src/",
			"lib/tinythread/",
			"lib/brofiler/",
			"lib/linmath.h/"
		}

		libdirs { DROID_LIB_ROOT }

		links {
			"droid_static",
			"TinyThread",
			"Comctl32" 
		}

		configuration {"windows"}
			defines { "_ITERATOR_DEBUG_LEVEL=0" }

		configuration {"windows","debug","vs2008"}
			links {"bgfxDebug_vs2008","lua51_vs2008","psapi"}

		configuration { "windows", "debug","vs2015" }
			links { "bgfxDebug_vs2015","lua51" }

		configuration { "windows", "debug","vs2017" }
			links { "bgfxDebug_vs2015","lua51","psapi" }

		configuration { "windows", "x32" }
			libdirs {
				DROID_ROOT .. "lib/brofiler/x86/",
				DROID_ROOT .. "lib/lua/x86/",
				DROID_ROOT .. "lib/bgfx/x86/" -- todo: debug/release versions
			}

			links { "ProfilerCore32" }

		configuration { "windows", "x64" }
			libdirs { 
				DROID_ROOT .. "lib/brofiler/x64/",
				DROID_ROOT .. "lib/lua/x64/"
			}

			links { "ProfilerCore64" }

		-- configuration "windows"
  --   		postbuildcommands { "echo d | xcopy /C /E /I /y data ..\\bin\\data" }

		-- configuration "not windows"
  --   		postbuildcommands { "cp data ../bin/data" }

	copy_assets(self)
end

return module