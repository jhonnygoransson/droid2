print("Droid Api test")
print("--------------")

print("Droid table contents:")

for k,v in pairs(droid) do
	print( "droid[" .. k .. "]", v )
end

print("Testing a registered function")
droid.func0()

print("Testing the log function")
droid.log()