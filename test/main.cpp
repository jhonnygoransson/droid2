#include <assert.h>

#include "droid.h"
#include "droid_interfaces.h"

#include "core/droid_worker_manager.h"
#include "core/file_system/droid_file_memory.h"
#include "core/file_system/droid_file_disk.h"
#include "core/droid_function.h"
#include "core/droid_logger.h"
#include "core/droid_configurator.h"
#include "core/droid_introspection.h"
#include "core/droid_profiling.h"
#include "core/droid_event.h"

#include "game/component/droid_game_component_system.h"
#include "game/component/droid_game_component.h"
#include "game/component/droid_game_entity.h"

#include "script/droid_script_system.h"

#include "graphics/droid_graphics_system.h"
#include "graphics/droid_vertex_declaration.h"
#include "graphics/droid_render_state.h"
#include "graphics/droid_viewport.h"

#include "util/droid_platform_context.h"

#include "math/droid_math.h"

Droid::Engine* engine = 0;
Droid::Logger* logger = 0;

void testArray()
{
	Droid::Array<int> _arr;

	int _arrSize = 4;

	_arr.Resize(_arrSize);

	for (int i = 0; i < _arrSize; ++i)
	{
		int _v = _arr[i];

		assert(_v == 0);
	}

	_arr.Resize(_arrSize);

	_arr.Resize(0);

	_arr.Push(1);
	_arr.Push(2);
	_arr.Resize(_arrSize);

	int v0 = _arr[0];
	int v1 = _arr[1];
	int v2 = _arr[2];
	int v3 = _arr[3];

	_arr.Resize(2);

	v0 = _arr[0];
	v1 = _arr[1];
}

void testFileMemory()
{
	Droid::FileMemory file;
	Droid::Path _path("test/path");

	char _bytes[] = "test_data";

	file.Open(_path,Droid::FileSystem::WRITE);
	file.Write(_bytes, sizeof(_bytes));
	file.Close();

	char _bytesRead[10];
	memset(_bytesRead,0,10);

	file.Open(_path,Droid::FileSystem::READ);
	file.Read(_bytesRead,5);
	file.Read(_bytesRead,5);
	file.Close();
}

void testFileDisk()
{
	char* _bytesDisk = 0;

	Droid::FileDisk fileDisk;
	Droid::Path _pathDisk("e:/tmp/test_read.txt");

	fileDisk.Open(_pathDisk, Droid::FileSystem::READ | Droid::FileSystem::WRITE );

	size_t _sizeDisk = fileDisk.Size();
	_bytesDisk = (char*) malloc( sizeof(char) * _sizeDisk );

	fileDisk.Read( _bytesDisk, 5 );
	fileDisk.Read( _bytesDisk + 5, 5 );
	fileDisk.Read( _bytesDisk + 10, _sizeDisk - 10 );

	fileDisk.Write( _bytesDisk, 10 );
	fileDisk.Seek( Droid::File::BEGIN, 5 );

	char _bytesDiskToWrite[3] = "!!";

	fileDisk.Write( _bytesDiskToWrite, 2 );

	fileDisk.Close();

	DROID_FREE_PTR(_bytesDisk);
}

void testBufferedMessages()
{
	Droid::WorkerManager* wm = (Droid::WorkerManager*) engine->GetSystem("WorkerManager");

	wm->Send("Test0");
	wm->Send("Test1");
	wm->Send("Test2");
	wm->Send("Test3");
	wm->Send("Test4");
	wm->Send("Test5");

	wm->CreateWorkers(2);
}

void testLoadAsyncFile()
{
	Droid::FileSystem* _fs = (Droid::FileSystem*) engine->GetSystem("FileSystem");
	_fs->OpenAsync( Droid::FileSystem::DISK, "e:/tmp/test_read.txt", Droid::FileSystem::READ ); 
}

void testFunctorsCallback()
{
	logger->Log( DROID_LOG_INFO,"Test","testFunctors called");
}

void testFunctorsCallbackWithOneArg( int _arg0 )
{
	logger->Log( DROID_LOG_INFO,"Test","testFunctorsCallbackWithOneArg called, received argument '%i'", _arg0);
}

void testFunctorsCallbackWithOneFloatArg( float _arg0 )
{
	logger->Log( DROID_LOG_INFO,"Test","testFunctorsCallbackWithOneFloatArg called, received argument '%f'", _arg0);	
}

void testFunctorsCallbackWithOneCharPtrArg( const char* _arg0 )
{
	logger->Log( DROID_LOG_INFO,"Test","testFunctorsCallbackWithOneCharPtrArg called, received argument '%s'", _arg0);	
}

void testFunctorsCallbackWithOneBoolPtrArg( bool _arg0 )
{
	int _argAsInt = _arg0 ? 1 : 0;
	logger->Log( DROID_LOG_INFO,"Test","testFunctorsCallbackWithOneBoolPtrArg called, received argument '%i'", _argAsInt);	
}

void testFunctorsCallbackWithTwoCharPtrArgs( const char* _arg0, const char* _arg1 )
{
	logger->Log( DROID_LOG_INFO,"Test","testFunctorsCallbackWithTwoCharPtrArgs called, received arguments '%s', '%s'", _arg0, _arg1);
}

void testScriptSystem()
{
	Droid::Path _scriptPath("data/scripts/api_test.lua");
	Droid::FileDisk _scriptFile(_scriptPath);
	
	Droid::ScriptSystem* _sc = (Droid::ScriptSystem*) engine->GetSystem("ScriptSystem");

	static Droid::Function function = Droid::Function::Build<void(*)(void),&testFunctorsCallback>(&testFunctorsCallback);
	_sc->RegisterLuaFunction( "func0", &function );

	static Droid::Function functionWithArg = Droid::Function::Build<void(*)(int),&testFunctorsCallbackWithOneArg,int>(&testFunctorsCallbackWithOneArg);
	_sc->RegisterLuaFunction( "func1", &functionWithArg);

	static Droid::Function functionWithFloatArg = Droid::Function::Build<void(*)(float),&testFunctorsCallbackWithOneFloatArg,float>(&testFunctorsCallbackWithOneFloatArg);
	_sc->RegisterLuaFunction( "func2", &functionWithFloatArg);

	static Droid::Function functionWithCharPtrArg = Droid::Function::Build<void(*)(const char*),&testFunctorsCallbackWithOneCharPtrArg, const char*>(&testFunctorsCallbackWithOneCharPtrArg);
	_sc->RegisterLuaFunction( "func3", &functionWithCharPtrArg);

	static Droid::Function functionWithTwoCharPtrArg = Droid::Function::Build<void(*)(const char*, const char*),&testFunctorsCallbackWithTwoCharPtrArgs, const char*, const char*>(&testFunctorsCallbackWithTwoCharPtrArgs);
	_sc->RegisterLuaFunction( "func5", &functionWithTwoCharPtrArg);

	static Droid::Function functionWithBoolPtrArg = Droid::Function::Build<void(*)(bool),&testFunctorsCallbackWithOneBoolPtrArg, bool>(&testFunctorsCallbackWithOneBoolPtrArg);
	_sc->RegisterLuaFunction( "func4", &functionWithBoolPtrArg);

	Droid::Script* _script1  = _sc->Load(_scriptFile);
}

class TestFunctorClass
{
public:
	TestFunctorClass() : m_value(1) {}
	~TestFunctorClass() {}

	void TestFunction() {
		m_value++;
	}

	void TestFunctionWithIntArg(int arg0) {
		m_value += arg0;
	}

	void TestFunctionWithTwoIntArgs(int arg0, int arg1) {
		m_value += arg0;
		m_value += arg1;
	}

private:
	int m_value;
};

void testFunctors()
{
	// simple empty functor
	Droid::Function function = Droid::Function::Build<void(*)(void),&testFunctorsCallback>(&testFunctorsCallback);
	function();

	Droid::Function functionWithArg = Droid::Function::Build<void(*)(int),&testFunctorsCallbackWithOneArg,int>(&testFunctorsCallbackWithOneArg);
	functionWithArg( 1 );

	// function inside class
	Droid::Function functionInClass = Droid::Function::Build<void(TestFunctorClass::*)(void),&TestFunctorClass::TestFunction>(&TestFunctorClass::TestFunction);
	Droid::Function functionInClassWithIntArg = Droid::Function::Build<void(TestFunctorClass::*)(int),&TestFunctorClass::TestFunctionWithIntArg>(&TestFunctorClass::TestFunctionWithIntArg);
	Droid::Function functionInClassWithTwoIntArgs = Droid::Function::Build<void(TestFunctorClass::*)(int,int),&TestFunctorClass::TestFunctionWithTwoIntArgs>(&TestFunctorClass::TestFunctionWithTwoIntArgs);

	TestFunctorClass testClass;
	
	functionInClass.Bind( testClass );
	functionInClassWithIntArg.Bind( testClass );
	functionInClassWithTwoIntArgs.Bind( testClass );

	functionInClass();

	functionInClassWithIntArg( 2 );

	functionInClassWithTwoIntArgs( 1, 2 );
}

void testConfigurator()
{
	Droid::Configurator* _config = static_cast<Droid::Configurator*>(engine->GetSystem("Configurator"));

	const char* _strValue = _config->GetConfigAsString( "TestString" );
	int _intValue         = _config->GetConfigAsInt( "TestInt" );
	float _floatValue     = _config->GetConfigAsFloat( "TestFloat" );
	bool _boolValue       = _config->GetConfigAsBool( "TestBool" );
}

class TestGameComponent : public Droid::GameComponent
{
public:
	TestGameComponent()
	: m_data(0){}

private:
	int m_data;
};

/*
class TestGameComponentScheme : public Droid::GameComponentScheme 
{
public:
	void Update()
	{}
};
*/

void testComponentSystem()
{
 	Droid::GameComponentSystem* _gcs = static_cast<Droid::GameComponentSystem*>(engine->GetSystem("GameComponentSystem"));

	_gcs->AllocateEntities(8);

	Droid::GameComponentType _gcsTestType = _gcs->AllocateComponent<TestGameComponent>();

	// _gcs->AddGameComponentScheme()

	TestGameComponent* _gcsTestType0 = _gcs->GetComponent<TestGameComponent>(0);

 	Droid::GameComponentType _gcsType0 = _gcs->CreateComponentType();
 	Droid::GameComponentType _gcsType1 = _gcs->CreateComponentType();
 	Droid::GameComponentType _gcsType2 = _gcs->CreateComponentType();
 	Droid::GameComponentType _gcsType3 = _gcs->CreateComponentType();

 	Droid::GameEntity& _gcsEntity0 = _gcs->NewEntity();

 	assert( _gcsEntity0 != Droid::GameEntity::NONE );

 	_gcsEntity0.AddComponent( _gcsType0 );
 	_gcsEntity0.AddComponent( _gcsType1 );
 	_gcsEntity0.AddComponent( _gcsType2 );

 	bool _hasComponent0, _hasComponent1, _hasComponent2, _hasComponent3;

 	_hasComponent0 = _gcsEntity0.HasComponent( _gcsType0 );
 	_hasComponent1 = _gcsEntity0.HasComponent( _gcsType1 );
 	_hasComponent2 = _gcsEntity0.HasComponent( _gcsType2 );
 	_hasComponent3 = _gcsEntity0.HasComponent( _gcsType3 );

 	assert( _hasComponent0 && _hasComponent1 && _hasComponent2 && !_hasComponent3 );

 	_gcsEntity0.RemoveComponent( _gcsType0 );
 	_gcsEntity0.RemoveComponent( _gcsType1 );

 	_hasComponent0 = _gcsEntity0.HasComponent( _gcsType0 );
 	_hasComponent1 = _gcsEntity0.HasComponent( _gcsType1 );
 	_hasComponent2 = _gcsEntity0.HasComponent( _gcsType2 );
 	_hasComponent3 = _gcsEntity0.HasComponent( _gcsType3 );

 	assert( !_hasComponent0 && !_hasComponent1 && _hasComponent2 && !_hasComponent3 );

 	_gcsEntity0.AddComponent( _gcsType3 );

 	_hasComponent0 = _gcsEntity0.HasComponent( _gcsType0 );
 	_hasComponent1 = _gcsEntity0.HasComponent( _gcsType1 );
 	_hasComponent2 = _gcsEntity0.HasComponent( _gcsType2 );
 	_hasComponent3 = _gcsEntity0.HasComponent( _gcsType3 );

 	assert( !_hasComponent0 && !_hasComponent1 && _hasComponent2 && _hasComponent3 );
}

typedef struct
{
	float vx;
	float vy;
	float vz;
	unsigned char  r;
	unsigned char  g;
	unsigned char  b;
	unsigned char  a;
} TestVertex;

class TestRenderer : public Droid::Subscriber, Droid::ViewportRenderer
{
public:
	void CreateAssets(Droid::GraphicsSystem* graphicsSystem)
	{
		gfx = graphicsSystem;

		Droid::FileDisk fileDisk;
		Droid::Path _vsPath("C:/dev/droid2/test/vs_cubes.bin");
		Droid::Path _fsPath("C:/dev/droid2/test/fs_cubes.bin");

		fileDisk.Open(_vsPath, Droid::FileSystem::READ);
		vsHandle = gfx->CreateShader(fileDisk);
		fileDisk.Close();

		fileDisk.Open(_fsPath, Droid::FileSystem::READ);
		fsHandle = gfx->CreateShader(fileDisk);
		fileDisk.Close();

		programHandle = gfx->CreateProgram(vsHandle,fsHandle);
		
		const TestVertex testVertexData[4] = 
		{
		    0.5f,  0.5f, 0.0f,0xFF,0xFF,0,0xFF,
		   -0.5f, -0.5f, 0.0f,0,0,0,0xFF,
		    0.5f, -0.5f, 0.0f,0xFF,0,0,0xFF,
		   -0.5f,  0.5f, 0.0f,0,0xFF,0,0xFF
		};

		const unsigned short int testVertexIndexData[6] = 
		{
			0, 2, 1,
			0, 1, 3
		};

		Droid::VertexDeclaration* vDecl = Droid::VertexDeclaration::Create();
		vDecl->AddAttribute( Droid::Attribute::Position, 3, Droid::AttributeType::Float);
		vDecl->AddAttribute( Droid::Attribute::Color0, 4, Droid::AttributeType::UInt8,true);
		vDecl->Commit();

		// vertex buffer
		{
			Droid::FileMemory testVertexDataFile;
			testVertexDataFile.Open(Droid::FileSystem::WRITE | Droid::FileSystem::READ);
			testVertexDataFile.Write((char*) testVertexData, sizeof(testVertexData));
			vbHandle = gfx->CreateVertexBuffer(vDecl, testVertexDataFile);
			testVertexDataFile.Close();
		}

		// index buffer
		{
			Droid::FileMemory testIndexDataFile;
			testIndexDataFile.Open(Droid::FileSystem::WRITE | Droid::FileSystem::READ);
			testIndexDataFile.Write((char*) testVertexIndexData, sizeof(testVertexIndexData));
			ibHandle = gfx->CreateIndexBuffer(testIndexDataFile);
			testIndexDataFile.Close();
		}

		viewport = gfx->CreateViewport();
		viewport->SetRect(Droid::WindowRect());
		viewport->SetViewportRenderer(this);
		
		engine->AddSubscriber(this);
		gfx->AddSubscriber(this);
	}

private:
	void Render(Droid::Viewport* vp, double dt)
	{
		Droid::Mat4x4 viewTransform,projectionTransform,modelTransform;
		Droid::Mat4x4Identity(viewTransform);
		Droid::Mat4x4Identity(modelTransform);
		Droid::Mat4x4Ortho(projectionTransform, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f);

		gfx->SetViewTransform(vp->GetId(),viewTransform,projectionTransform);
		gfx->SetModelTransform(modelTransform);

		gfx->SetVertexBuffer(vbHandle);
		gfx->SetIndexBuffer(ibHandle);

		gfx->SetState(Droid::RenderState::Default);
		gfx->Submit(vp->GetId(),programHandle);
	}

	void OnEvent(const Droid::Event evt)
	{
		if (evt == Droid::WindowEvents::Resize )
		{
			Droid::Configurator* _config = static_cast<Droid::Configurator*>(engine->GetSystem("Configurator"));

			int width  = _config->GetConfigAsInt("WindowWidth");
			int height = _config->GetConfigAsInt("WindowHeight");

			viewport->SetRect(Droid::WindowRect(0,0,width,height));
		}
	}

	Droid::ShaderHandle*       vsHandle;
	Droid::ShaderHandle*       fsHandle;
	Droid::ProgramHandle*      programHandle;
	Droid::VertexBufferHandle* vbHandle;
	Droid::IndexBufferHandle*  ibHandle;
	Droid::GraphicsSystem*     gfx;
	Droid::Viewport*           viewport;
};

void testGraphicsSystem()
{
	Droid::GraphicsSystem* gfx = static_cast<Droid::GraphicsSystem*>(engine->GetSystem("GraphicsSystem"));
	TestRenderer* testRenderer = new TestRenderer();
	testRenderer->CreateAssets(gfx);
}

class EventBase : public Droid::Publisher
{
protected:
	enum Events
	{
		Evt0=1,
		Evt1,
		Evt2
	};
};

class TestPublisher0 : public EventBase
{
public:
	void Run() { Notify(Evt0); }
};

class TestPublisher1 : public EventBase
{
public:
	void Run() { Notify(Evt1); }
};

class TestSubscriber : public Droid::Subscriber
{
public:
	TestSubscriber(int id)
	: m_id(id){}

	void OnEvent(const Droid::Event evt)
	{
		logger->Log( DROID_LOG_INFO,"Test","TestSubscriber[%i] received event %i",m_id,(int)evt);
	}

	int m_id;
};

void testEvent()
{
	TestPublisher0 pub0;
	TestPublisher1 pub1;
	TestSubscriber sub0(0),sub1(1),sub2(2);

	// test 1 - adding
	{
		pub0.AddSubscriber(&sub0);
		pub0.AddSubscriber(&sub1);
		pub0.AddSubscriber(&sub2);
		// shouldn't add twice
		pub0.AddSubscriber(&sub0);
	}

	// test 2 - Running
	{
		// 0,1,2 should all get one event
		pub0.Run();
	}

	// test 3 - remove one
	{
		pub0.RemoveSubscriber(&sub0);
		// shoulnd't remove twice
		pub0.RemoveSubscriber(&sub0);

		// 1,2 should get one event
		pub0.Run();
		pub0.RemoveSubscriber(&sub1);

		// 2 should get one event
		pub0.Run();
		pub0.RemoveSubscriber(&sub2);
		pub0.Run();
	}

	pub0.AddSubscriber(&sub0);
	pub0.AddSubscriber(&sub1);

	pub1.AddSubscriber(&sub0);
	pub1.AddSubscriber(&sub2);

	pub0.Run();
	pub1.Run();

	pub0.RemoveAllSubscribers();
	pub1.RemoveAllSubscribers();
}

int main(int argc, char const *argv[])
{
	Droid::PlatformContext ctx;

	engine = Droid::Engine::Create(ctx);
	logger = static_cast<Droid::Logger*>(engine->GetSystem("Logger"));

	engine->Attach(Droid::ScriptSystem::Create());
	engine->Attach(Droid::GraphicsSystem::Create());
	engine->Attach(Droid::GameComponentSystem::Create());
	engine->Initialize();

	// testArray();
	// testFileMemory();
	// testFileDisk();
	// testBufferedMessages();
	// testLoadAsyncFile();
	// testScriptSystem();
	// testFunctors();
	// testConfigurator();
	// testComponentSystem();
	testGraphicsSystem();
	// testEvent();

	while( engine->GetEngineState() == Droid::Running )
	{ 
		DROID_PF_FRAME("MAIN")

		engine->PumpMessages();
		engine->Update(0.0);
	}

	Droid::Engine::Destroy();
}