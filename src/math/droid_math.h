#pragma once

#include <linmath.h>

namespace Droid
{
	// Additions for v2 math:
	typedef float  Vec2[2];
	typedef vec3   Vec3;
	typedef vec4   Vec4;
	typedef mat4x4 Mat4x4;
	typedef quat   Quat;

	static __inline void Vec2Add(Vec2 r, Vec2 const a, Vec2 const b)
	{
		int i;
		for(i=0; i<2; ++i)
			r[i] = a[i] + b[i];
	}
	static __inline void Vec2Sub(Vec2 r, Vec2 const a, Vec2 const b)
	{
		int i;
		for(i=0; i<2; ++i)
			r[i] = a[i] - b[i];
	}
	static __inline void Vec2Scale(Vec2 r, Vec2 const v, float const s)
	{
		int i;
		for(i=0; i<2; ++i)
			r[i] = v[i] * s;
	}
	static __inline float Vec2MulInner(Vec2 const a, Vec2 const b)
	{
		float p = 0.f;
		int i;
		for(i=0; i<2; ++i)
			p += b[i]*a[i];
		return p;
	}
	static __inline void Vec2MulCross(Vec2 r, Vec2 const a, Vec2 const b)
	{
		r[0] = a[1]*b[2] - a[2]*b[1];
		r[1] = a[2]*b[0] - a[0]*b[2];
	}
	static __inline float Vec2Len(Vec2 const v)
	{
		return sqrtf(Vec2MulInner(v, v));
	}
	static __inline void Vec2Norm(Vec2 r, Vec2 const v)
	{
		float k = 1.f / Vec2Len(v);
		Vec2Scale(r, v, k);
	}
	static __inline void Vec2Reflect(Vec2 r, Vec2 const v, Vec2 const n)
	{
		float p  = 2.f*Vec2MulInner(v, n);
		int i;
		for(i=0;i<2;++i)
			r[i] = v[i] - p*n[i];
	}

	static __inline void Vec3Add(Vec3 r, Vec3 const a, Vec3 const b) { vec3_add(r,a,b); }
	static __inline void Vec3Sub(Vec3 r, Vec3 const a, Vec3 const b) { vec3_sub(r,a,b); }
	static __inline void Vec3Scale(Vec3 r, Vec3 const v, float const s) { vec3_scale(r,v,s); }
	static __inline float Vec3MulInner(Vec3 const a, Vec3 const b) { return vec3_mul_inner(a,b); }
	static __inline void Vec3MulCross(Vec3 r, Vec3 const a, Vec3 const b) { vec3_mul_cross(r,a,b); }
	static __inline float Vec3Len(Vec3 const v) { return vec3_len(v); }
	static __inline void Vec3Norm(Vec3 r, Vec3 const v) { vec3_norm(r,v); }
	static __inline void Vec3Reflect(Vec3 r, Vec3 const v, Vec3 const n) { vec3_reflect(r,v,n); }
	static __inline void Vec4Add(Vec4 r, Vec4 const a, Vec4 const b) { vec4_add(r,a,b); }
	static __inline void Vec4Sub(Vec4 r, Vec4 const a, Vec4 const b) { vec4_sub(r,a,b); }
	static __inline void Vec4Scale(Vec4 r, Vec4 v, float s) { vec4_scale(r,v,s); }
	static __inline float Vec4MulInner(Vec4 a, Vec4 b) { return vec4_mul_inner(a,b); }
	static __inline void Vec4MulCross(Vec4 r, Vec4 a, Vec4 b) { vec4_mul_cross(r,a,b); }
	static __inline float Vec4Len(Vec4 v) { return vec4_len(v); }
	static __inline void Vec4Norm(Vec4 r, Vec4 v) { vec4_norm(r,v); }
	static __inline void Vec4Reflect(Vec4 r, Vec4 v, Vec4 n) { vec4_reflect(r,v,n); }
	static __inline void Mat4x4Identity(Mat4x4 M) { mat4x4_identity(M); }
	static __inline void Mat4x4Dup(Mat4x4 M, Mat4x4 N) { mat4x4_dup(M,N); }
	static __inline void Mat4x4Row(Vec4 r, Mat4x4 M, int i) { mat4x4_row(r,M,i); }
	static __inline void Mat4x4Col(Vec4 r, Mat4x4 M, int i) { mat4x4_col(r,M,i); }
	static __inline void Mat4x4Transpose(Mat4x4 M, Mat4x4 N) { mat4x4_transpose(M,N); }
	static __inline void Mat4x4Add(Mat4x4 M, Mat4x4 a, Mat4x4 b) { mat4x4_add(M,a,b); }
	static __inline void Mat4x4Sub(Mat4x4 M, Mat4x4 a, Mat4x4 b) { mat4x4_sub(M,a,b); }
	static __inline void Mat4x4Scale(Mat4x4 M, Mat4x4 a, float k) { mat4x4_scale(M,a,k); }
	static __inline void Mat4x4ScaleAniso(Mat4x4 M, Mat4x4 a, float x, float y, float z) { mat4x4_scale_aniso(M,a,x,y,z); }
	static __inline void Mat4x4Mul(Mat4x4 M, Mat4x4 a, Mat4x4 b) { mat4x4_mul(M,a,b); }
	static __inline void Mat4x4MulVec4(Vec4 r, Mat4x4 M, Vec4 v) { mat4x4_mul_vec4(r,M,v); }
	static __inline void Mat4x4Translate(Mat4x4 T, float x, float y, float z) { mat4x4_translate(T,x,y,z); }
	static __inline void Mat4x4TranslateInPlace(Mat4x4 M, float x, float y, float z) { mat4x4_translate_in_place(M,x,y,z); }
	static __inline void Mat4x4FromVec3MulOuter(Mat4x4 M, Vec3 a, Vec3 b) { mat4x4_from_vec3_mul_outer(M,a,b); }
	static __inline void Mat4x4Rotate(Mat4x4 R, Mat4x4 M, float x, float y, float z, float angle) { mat4x4_rotate(R,M,x,y,z,angle); }
	static __inline void Mat4x4RotateX(Mat4x4 Q, Mat4x4 M, float angle) { mat4x4_rotate_X(Q,M,angle); }
	static __inline void Mat4x4RotateY(Mat4x4 Q, Mat4x4 M, float angle) { mat4x4_rotate_Y(Q,M,angle); }
	static __inline void Mat4x4RotateZ(Mat4x4 Q, Mat4x4 M, float angle) { mat4x4_rotate_Z(Q,M,angle); }
	static __inline void Mat4x4Invert(Mat4x4 T, Mat4x4 M) { mat4x4_invert(T,M); }
	static __inline void Mat4x4Orthonormalize(Mat4x4 R, Mat4x4 M) { mat4x4_orthonormalize(R,M); }
	static __inline void Mat4x4Frustum(Mat4x4 M, float l, float r, float b, float t, float n, float f) { mat4x4_frustum(M,l,r,b,t,n,f); }
	static __inline void Mat4x4Ortho(Mat4x4 M, float l, float r, float b, float t, float n, float f) { mat4x4_ortho(M,l,r,b,t,n,f); }
	static __inline void Mat4x4Perspective(Mat4x4 m, float y_fov, float aspect, float n, float f) { mat4x4_perspective(m,y_fov,aspect,n,f); }
	static __inline void Mat4x4LookAt(Mat4x4 m, Vec3 eye, Vec3 center, Vec3 up) { mat4x4_look_at(m,eye,center,up); }
	static __inline void QuatIdentity(Quat q) { quat_identity(q); }
	static __inline void QuatAdd(Quat r, Quat a, Quat b) { quat_add(r,a,b); }
	static __inline void QuatSub(Quat r, Quat a, Quat b) { quat_sub(r,a,b); }
	static __inline void QuatMul(Quat r, Quat p, Quat q) { quat_mul(r,p,q); }
	static __inline void QuatScale(Quat r, Quat v, float s) { quat_scale(r,v,s); }
	static __inline float QuatInnerProduct(Quat a, Quat b) { return quat_inner_product(a,b); }
	static __inline void QuatConj(Quat r, Quat q) { quat_conj(r,q); }
	static __inline void QuatMulVec3(Vec3 r, Quat q, Vec3 v) { quat_mul_vec3(r,q,v); }
	static __inline void Mat4x4FromQuat(Mat4x4 M, Quat q) { mat4x4_from_quat(M,q); }
	static __inline void Mat4x4oMulQuat(mat4x4 R, mat4x4 M, Quat q) { mat4x4o_mul_quat(R,M,q); }
	static __inline void QuatFromMat4x4(Quat q, mat4x4 M) { quat_from_mat4x4(q,M); }
}