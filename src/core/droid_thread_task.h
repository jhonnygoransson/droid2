#pragma once

namespace Droid
{
	class ThreadTask
	{
	public:
		ThreadTask() {}
		virtual ~ThreadTask() {}
		virtual bool Run() = 0;
		virtual const char* GetName() = 0;
	};
}