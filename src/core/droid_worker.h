#pragma once

#include "droid_msg_queue.h"
#include "tinythread.h"

namespace Droid
{
	class Worker
	{
	public:
		Worker();
		Worker(MsgQueue* _managerMsq, unsigned int _identifier);
		virtual ~Worker();
		virtual void Run();
		inline bool Worker::IsFree() { return m_free; }

	protected:
		friend class WorkerManager;
		inline MsgQueue* Worker::Msq() { return &m_msq; }
	private:
		MsgQueue         m_msq;
		MsgQueue*        m_managerMsq;
		tthread::thread* m_thread;
		unsigned int     m_identifier;
		bool             m_free;
	};
}