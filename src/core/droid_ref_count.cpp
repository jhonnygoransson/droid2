#include <assert.h>

#include "droid_ref_count.h"

using namespace Droid;


RefCount::RefCount() 
	: m_refCount(0) {}

RefCount::~RefCount() 
{
	assert(m_refCount == 0);
}

void RefCount::IncRef()
{
	m_refCount++;
}

void RefCount::DecRef()
{
	assert(m_refCount > 0);
	m_refCount--;
}