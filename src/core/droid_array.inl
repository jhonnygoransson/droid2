#pragma warning(disable: 4700)

template<class T>
Array<T>::Array()
	: m_data(0)
	, m_size(0)
	, m_byteSize(0)
	, m_capacity(m_arrayInitialSize)
{}

template<class T>
Array<T>::~Array()
{
	DROID_FREE_PTR(m_data);
}


template<class T>
T& Array<T>::operator [] (unsigned int _index)
{
	assert( m_data );
	assert( _index < m_size );

	return m_data[_index];
}

// allocates or reallocates memory based on _size arg
template<class T>
void Array<T>::_AllocateCapacity(unsigned int _size)
{
	assert( _size > 0 );

	size_t _elemSize = sizeof(T);

	if ( m_data )
	{
		m_data = (T*) realloc( m_data, _elemSize * _size );
	}
	else 
	{
		m_data = (T*) malloc( _elemSize * _size );
	}

	m_capacity = _size;
}

// sets array size explicitly
template<class T>
void Array<T>::Resize(unsigned int _size) 
{
	if ( _size == m_size ) 
		return;
	else if(_size == 0)
		Reset();
	else 
	{	
		size_t _elemSize = sizeof(T);

		_AllocateCapacity( _size );

		// zero-fill new data
		if ( _size > m_size )
		{
			T* _ptr = m_data + m_size;
			memset( (void*) _ptr, 0, _elemSize * _size - m_byteSize );
		}

		m_size     = _size;
		m_byteSize = _elemSize * _size;
	}
}

template<class T>
void Array<T>::Reset() 
{
	DROID_FREE_PTR(m_data);
	
	m_size     = 0;
	m_byteSize = 0;
}


template<class T>
void Array<T>::_Init()
{
	Reset();

	_AllocateCapacity(m_arrayInitialSize);

	assert(m_data);
}

template<class T>
void Array<T>::Push(T _item)
{
	if(!m_data)
		_Init();

	if( m_size >= m_capacity )
	{

		_AllocateCapacity( m_capacity * m_arrayStepMultiplier );
	}

	m_data[m_size] = _item;

	m_size++;
	m_byteSize = m_size * sizeof(T);
}

template<class T>
T Array<T>::Pop()
{
	T _ele;

	if ( m_size == 0 ) return _ele;

	_ele = m_data[0];

	for(unsigned int i=0; i < m_size-1; i++)
	{
		m_data[i] = m_data[i+1];
	}

	m_size--;
	m_byteSize = m_size * sizeof(T);

	if (m_size <= 0) Reset();

	return _ele;
}

template<class T>
int Array<T>::IndexOf(T _item)
{
	for(unsigned int i=0; i < m_size; i++)
	{
		if ( m_data[i] == _item )
			return (int) i;
	}
	
	return -1;
}

template<class T>
void Array<T>::Reverse()
{
	unsigned int start = 0;
	unsigned int end   = m_size-1;

	while( start != end && end > start )
	{
		T tmp = m_data[start];
		m_data[start] = m_data[end];
		m_data[end]   = tmp;
	}
}

template<class T>
inline unsigned int Array<T>::Size() { return m_size; }