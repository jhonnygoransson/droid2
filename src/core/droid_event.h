#pragma once
#include "droid_common.h"
#include "droid_engine_events.h"

namespace Droid
{
	typedef unsigned int Event;

	class Subscriber
	{
	public:
		virtual void OnEvent(const Event evt);
	};

	class Publisher
	{
	public:
		void AddSubscriber(Subscriber* subscriber);
		void RemoveSubscriber(Subscriber* subscriber);
		void RemoveAllSubscribers();
	protected:
		void Notify(const Event evt);
	private:
		Array<Subscriber*> m_subscribers;
	};
}