#include "droid_engine.h"
#include "droid_worker_manager.h"
#include "droid_logger.h"
#include "droid_system.h"
#include "droid_configurator.h"
#include "droid_introspection.h"
#include "file_system/droid_file_system.h"
#include "script/droid_script_system.h"

using namespace Droid;

class EngineImpl : public Engine, public System
{
public:
	EngineImpl(const PlatformContext ctx) 
		: System("DroidEngine")
		, m_defaultSystemsAttached(false)
		, m_configurator(Configurator::Create())
		, m_introspection(Introspection::Create())
		, m_platformCtx(ctx)
		, m_engineState(UnInitialized)
	{
		SubscribeTo("RegisterScriptAPI");
	}

	~EngineImpl()
	{
		m_logger.Log(DROID_LOG_INFO,"Engine","Droid shutting down..");

		DetachAll();

		DROID_DEL_PTR(m_configurator);
		DROID_DEL_PTR(m_introspection);
	}

	bool OnEvent(const char* _event)
	{
		if ( strcmp(_event,"RegisterScriptAPI") == 0 )
		{
			m_logger.Log(DROID_LOG_INFO,"Engine","Registering Script API functions");
		}

		return true;
	}

	void Initialize()
	{
		TransitionTo(Running);

		Notify("EngineCreated");

		m_logger.Log(DROID_LOG_INFO,"Engine","Droid created..");
	}

	void Update(double _dt)
	{
		System::Update(_dt);
	}

	System* GetSystem(const char* _name)
	{
		return System::GetSystem(_name);
	}

	System* Attach(System* _system)
	{
		// first time someone attaches an external system, we must make sure
		// that we already have attached our internal systems
		if(!m_defaultSystemsAttached)
		{
			System::Attach(&m_logger);
			System::Attach(&m_workerManager);
			System::Attach(&m_fileSystem);
			System::Attach(m_introspection);
			System::Attach(m_configurator);

			m_defaultSystemsAttached = true;
		}

		Publisher::AddSubscriber(_system);

		return System::Attach(_system);
	}

	System* Detach(System* _system)
	{
		return System::Detach(_system);
	}

	void AddSubscriber(Subscriber* subscriber)
	{
		Publisher::AddSubscriber(subscriber);
	}

	void ConfigPath(const char* _path)
	{
		m_configurator->SetConfigPath(_path);
	}

	const PlatformContext& GetPlatformContext()
	{
		return m_platformCtx;
	}

	inline void TransitionTo(EngineState newState)
	{
		if (m_engineState != newState)
		{
			m_engineState = newState;
			// Publisher::Notify(EngineEvents::NewState);
		}
	}

	EngineState GetEngineState()
	{
		return m_engineState;
	}

	void Resize(unsigned int _width, unsigned int _height)
	{
		TypeInfo* intTypeInfo = Introspection::GetType<int>();

		Variable& maybeWidthVariable = m_configurator->GetConfigAsVariable("WindowWidth");
		Variable& maybeHeightVariable = m_configurator->GetConfigAsVariable("WindowHeight");

		if ( &maybeWidthVariable == &Configurator::ConfigValueNotFound )
		{
			unsigned int* newWidthValue = new unsigned int;
			*newWidthValue              = _width;
			Variable newWidthVariable(intTypeInfo,newWidthValue);
			m_configurator->SetConfigValue("WindowWidth",newWidthVariable);
		}
		else
		{
			*(static_cast<int*>(maybeWidthVariable.GetData())) = _width;
		}

		if ( &maybeHeightVariable == &Configurator::ConfigValueNotFound )
		{
			unsigned int* newHeightValue = new unsigned int;
			*newHeightValue              = _height;
			Variable newHeightVariable(intTypeInfo,newHeightValue);
			m_configurator->SetConfigValue("WindowHeight",newHeightVariable);
		}
		else
		{
			*(static_cast<int*>(maybeHeightVariable.GetData())) = _height;
		}

		Publisher::Notify(WindowEvents::Resize);
	}

	void PumpMessages()
	{
		Droid::MsgQueue_Msg msg = m_platformCtx.PollMessage();

		while(msg.msg)
		{
			if (strcmp(msg.msg,"WINDOW_CLOSE")==0)
			{
				TransitionTo(Stopped);
				break;
			}
			else if (strcmp(msg.msg,"WINDOW_RESIZE")==0)
			{
				Droid::PlatFormResizeMsg* resizeMsg = static_cast<Droid::PlatFormResizeMsg*>(msg.data);
	
				m_logger.Log( DROID_LOG_INFO,"Test","Window resized to %u %u",resizeMsg->w, resizeMsg->h);
				Resize(resizeMsg->w,resizeMsg->h);

				DROID_FREE_PTR(resizeMsg);
			}

			msg = m_platformCtx.PollMessage();
		}
	}

private:

	// internal modules
	Logger         m_logger;
	WorkerManager  m_workerManager;
	FileSystem     m_fileSystem;
	Introspection* m_introspection;
	Configurator*  m_configurator;

	// internal state 
	EngineState    m_engineState;
	bool           m_defaultSystemsAttached;

	const PlatformContext m_platformCtx;
};

Engine* Engine::m_engine = 0;

Engine* Engine::Create(const PlatformContext ctx)
{
	if(m_engine) return m_engine;

	EngineImpl* engine = new EngineImpl(ctx);
	m_engine = engine;

	return m_engine;
}

void Engine::Destroy()
{
	DROID_DEL_PTR(m_engine);
}

Engine* Engine::Get()
{
	return m_engine;
}
