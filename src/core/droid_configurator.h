#pragma once

#include "droid_interfaces.h"
#include "droid_system.h"
#include "droid_function.h"

namespace Droid
{
	class Configurator : public System
	{
	public:
		Configurator() : System("Configurator") {}
		virtual ~Configurator() {}

		virtual void SetConfigValue(const char* _key, const Variable& _variable) = 0;
		virtual void ReadConfig() = 0;
		virtual void SetConfigPath(const char* _path) = 0;
		virtual const char* GetConfigAsString( const char* _key, const char* _default = "") = 0;
		virtual int GetConfigAsInt( const char* _key, int _default = 0) = 0;
		virtual float GetConfigAsFloat( const char* _key, float _default = 0.0f ) = 0;
		virtual bool GetConfigAsBool( const char* _key, bool _default = false ) = 0;
		virtual Variable& GetConfigAsVariable( const char* _key ) = 0;

		static Configurator* Create();
		static Variable ConfigValueNotFound;
	};
}