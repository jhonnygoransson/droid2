#include <assert.h>

#include "droid_event.h"

namespace Droid
{
	void Subscriber::OnEvent(const Event evt)
	{
		// default empty
	}

	void Publisher::AddSubscriber(Subscriber* subscriber)
	{
		assert(subscriber);
	
		if (m_subscribers.IndexOf(subscriber)<0)
		{
			m_subscribers.Push(subscriber);
		}
	}

	void Publisher::RemoveSubscriber(Subscriber* subscriber)
	{
		assert(subscriber);
		int subscriberIndex = m_subscribers.IndexOf(subscriber);

		if(subscriberIndex >= 0)
		{
			m_subscribers[subscriberIndex] = 0;
			unsigned int numSystems = m_subscribers.Size();
			for(unsigned int i=subscriberIndex; i<numSystems; i++)
			{
				if( i+1 <= numSystems-1 )
				{
					Subscriber* tmp = m_subscribers[i+1];
					m_subscribers[i+1]  = m_subscribers[i];
					m_subscribers[i]    = tmp;
				}
			}

			m_subscribers.Resize(numSystems-1);
		}
	}

	void Publisher::RemoveAllSubscribers()
	{
		m_subscribers.Resize(0);
	}

	void Publisher::Notify(const Event evt)
	{
		for(unsigned int i=0;i<m_subscribers.Size();i++)
		{
			assert(m_subscribers[i]);
			m_subscribers[i]->OnEvent(evt);
		}
	}
}