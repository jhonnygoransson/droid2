#pragma once

#include <assert.h>
#include "droid_introspection.h"

namespace Droid
{
	// unused
	class FunctionSignature
	{
	public:

		inline unsigned GetArgCount() { return m_argCount; }
		inline const TypeInfo* GetArg(unsigned _index) { 
			assert( _index < m_argCount );
			return m_argsTypeInfo[_index]; 
		}

		FunctionSignature() 
		: m_argCount(0) 
		{}

		FunctionSignature(void (*)( void ))	
		: m_argCount(0)
		, m_argsTypeInfo(0)
		, m_context(0)
		{}

		template<typename Arg1>
		FunctionSignature(void (*)( Arg1 ))
		: m_argCount(1)
		, m_context(0)
		{
			// bind static argument typeinfo to function signature
			static const TypeInfo* _argsTypeInfo[] = 
			{
				Introspection::GetType<Arg1>()
			};

			m_argsTypeInfo = _argsTypeInfo;
		}

		template<typename Arg1, typename Arg2>
		FunctionSignature(void (*)( Arg1, Arg2 ))
		: m_argCount(2)
		, m_context(0)
		{
			// bind static argument typeinfo to function signature
			static const TypeInfo* _argsTypeInfo[] = 
			{
				Introspection::GetType<Arg1>(),
				Introspection::GetType<Arg2>()
			};

			m_argsTypeInfo = _argsTypeInfo;
		}

		template<typename C>
		FunctionSignature(void (C::*)( void ))	
		: m_argCount(0)
		, m_argsTypeInfo(0)
		{
			static const TypeInfo* _contextTypeInfo = 
				Introspection::GetType<C>();

			m_context = _contextTypeInfo;
		}

		template<typename C, typename Arg1>
		FunctionSignature(void (C::*)( Arg1 ))	
		: m_argCount(1)
		, m_argsTypeInfo(0)
		{
			static const TypeInfo* _contextTypeInfo = 
				Introspection::GetType<C>();

			m_context = _contextTypeInfo;

			static const TypeInfo* _argsTypeInfo[] = 
			{
				Introspection::GetType<Arg1>()
			};

			m_argsTypeInfo = _argsTypeInfo;
		}

		template<typename C, typename Arg1, typename Arg2>
		FunctionSignature(void (C::*)( Arg1, Arg2 ))	
		: m_argCount(2)
		, m_argsTypeInfo(0)
		{
			static const TypeInfo* _contextTypeInfo = 
				Introspection::GetType<C>();

			m_context = _contextTypeInfo;

			static const TypeInfo* _argsTypeInfo[] = 
			{
				Introspection::GetType<Arg1>(),
				Introspection::GetType<Arg2>()
			};

			m_argsTypeInfo = _argsTypeInfo;
		}

	private:
		unsigned m_argCount;
		const TypeInfo*  m_context;
		const TypeInfo** m_argsTypeInfo;
	};

	class Variable
	{
	public:
		Variable() {}
		Variable( const TypeInfo* _typeInfo, void* _data )
		: m_typeInfo(_typeInfo)
		, m_data(_data)
		{}

		template <typename T>
		Variable( const T& rhs )
		: m_data( (T*)&rhs )
		, m_typeInfo( ((Introspection*)Engine::Get()->GetSystem("Introspection"))->GetType<T>() )
		{}

		template <typename T>
		Variable( const T *rhs )
		: m_data( (T *)rhs )
		, m_typeInfo( ((Introspection*)Engine::Get()->GetSystem("Introspection"))->GetType<T>() )
		{}

		template <typename T>
		Variable::Variable( T *rhs )
		: m_data( (T *)rhs )
		, m_typeInfo( ((Introspection*)Engine::Get()->GetSystem("Introspection"))->GetType<T>() )
		{}

		template <typename T>
		struct CastHelper
		{
			static T& Cast( void *& data )
			{
				return *(T *&)(data);
			}
		};

		template <typename T>
		struct CastHelper<T *>
		{
			static T *& Cast( void *& data )
			{
				return (T *&)data;
			}
		};

		template <typename T>
		T& GetValue()
		{
			return CastHelper<T>::Cast( m_data );
		}

		template <typename T>
		const T& Variable::GetValue( void ) const
		{
			return CastHelper<T>::Cast( m_data );
		}

		void SetValue(void* _data)
		{
			m_data = _data;
		}

		const TypeInfo* GetTypeInfo() { return m_typeInfo; }
		void* GetData() { return m_data; }
		const void* GetData() const { return m_data; }
	private:
		void*           m_data;
		const TypeInfo* m_typeInfo;
	};

	class Function
	{
	public:
		Function() : m_context(0) {}
		~Function() {}

		// static function with no return and no arguments
		Function(
			void (*fn)(void),
			void (*helper)(Variable*, void*, Variable*, unsigned)) 
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// static function with no return and 1 argument
		template <typename Arg1>
		Function(
			void (*fn)(Arg1),
			void (*helper)(Variable*, void*, Variable*, unsigned)) 
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// static function with no return and 1 argument
		template <typename Arg1, typename Arg2>
		Function(
			void (*fn)(Arg1,Arg2),
			void (*helper)(Variable*, void*, Variable*, unsigned)) 
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// method with no return and no argument
		template <typename C>
		Function(
			void (C::*fn)(void),
			void (*helper)(Variable*, void*, Variable*, unsigned))
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// method with no return and 1 argument
		template <typename C, typename Arg1>
		Function(
			void (C::*fn)(Arg1),
			void (*helper)(Variable*, void*, Variable*, unsigned))
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// method with no return and 2 arguments
		template <typename C, typename Arg1, typename Arg2>
		Function(
			void (C::*fn)(Arg1, Arg2),
			void (*helper)(Variable*, void*, Variable*, unsigned))
		: m_sig( fn )
		, m_callHelper( helper )
		{}

		// void function without arguments or return
		template <typename FunctionType, FunctionType FunctionPointer>
		static Function Build( void (*fn)(void) )
		{
			return Function( fn, &CallVoid<FunctionType, FunctionPointer> );
		}

		// void function with 1 argument and no return
		template <typename FunctionType, FunctionType FunctionPointer, typename Arg1>
		static Function Build( void (*fn)(Arg1) )
		{
			return Function( fn, &CallVoid<FunctionType, FunctionPointer, Arg1> );
		}

		// void function with 1 argument and no return
		template <typename FunctionType, FunctionType FunctionPointer, typename Arg1, typename Arg2>
		static Function Build( void (*fn)(Arg1,Arg2) )
		{
			return Function( fn, &CallVoid<FunctionType, FunctionPointer, Arg1, Arg2> );
		}

		// void class method without arguments or return
		template <typename FunctionType, FunctionType FunctionPointer, typename C>
		static Function Build( void (C::*fn)(void) )
		{
			return Function( fn, &CallMethodVoid<FunctionType, FunctionPointer, C> );
		}

		// void class method with one arguments and no return type
		template <typename FunctionType, FunctionType FunctionPointer, typename C, typename Arg1>
		static Function Build( void (C::*fn)(Arg1) )
		{
			return Function( fn, &CallMethodVoid<FunctionType, FunctionPointer, C, Arg1> );
		}

		// void class method with one arguments and no return type
		template <typename FunctionType, FunctionType FunctionPointer, typename C, typename Arg1, typename Arg2>
		static Function Build( void (C::*fn)(Arg1, Arg2) )
		{
			return Function( fn, &CallMethodVoid<FunctionType, FunctionPointer, C, Arg1, Arg2> );
		}

		void operator( )( void ) const
		{
			m_callHelper( 0, m_context, 0, 0 );
		}

		void operator() (Variable& _ret, Variable* _arguments, unsigned _numArguments )
		{
			m_callHelper( 0, m_context, _arguments, _numArguments );
		}

		template <typename Arg1>
		void operator( )( Arg1 arg1 ) const
		{
			Variable variables[1];

			new (variables) Variable( arg1 );
			
			m_callHelper( 0, m_context, variables, 1 );
		}

		template <typename Arg1, typename Arg2>
		void operator( )( Arg1 arg1, Arg2 arg2 ) const
		{
			Variable variables[2];

			new (variables) Variable( arg1 );
			new (variables+1) Variable( arg2 );
			
			m_callHelper( 0, m_context, variables, 2 );
		}

		// bind caller context from pointer
		template <typename C>
		void Bind( C* _context )
		{
			m_context = _context;
		}

		// bind caller context from reference
		template <typename C>
		void Bind( C& _context )
		{
			m_context = &_context;
		}

		FunctionSignature* GetSignature() { return &m_sig; }

	private:
		void* m_context;
		FunctionSignature m_sig;
		void (*m_callHelper)( Variable*, void*, Variable*, unsigned );

		template <typename FunctionType, FunctionType FunctionPtr>
		static void CallVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments) 
		{
			(*FunctionPtr)( );
		}

		template <typename FunctionType, FunctionType FunctionPtr, typename Arg1>
		static void CallVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments)
		{
			(*FunctionPtr)( _arguments[0].GetValue<Arg1>() );
		}

		template <typename FunctionType, FunctionType FunctionPtr, typename Arg1, typename Arg2>
		static void CallVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments)
		{
			(*FunctionPtr)( _arguments[0].GetValue<Arg1>(), _arguments[1].GetValue<Arg2>() );
		}

		template <typename FunctionType, FunctionType FunctionPtr, typename C>
		static void CallMethodVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments)
		{
			(((C*) _context)->*FunctionPtr)( );
		}

		template <typename FunctionType, FunctionType FunctionPtr, typename C, typename Arg1>
		static void CallMethodVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments)
		{
			(((C*) _context)->*FunctionPtr)( _arguments[0].GetValue<Arg1>() );
		}

		template <typename FunctionType, FunctionType FunctionPtr, typename C, typename Arg1, typename Arg2>
		static void CallMethodVoid(Variable* _returnType, void* _context, Variable* _arguments, unsigned _numArguments)
		{
			(((C*) _context)->*FunctionPtr)( _arguments[0].GetValue<Arg1>(), _arguments[1].GetValue<Arg2>() );
		}
	};
}