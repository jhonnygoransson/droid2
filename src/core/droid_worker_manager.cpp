#include "droid_worker_manager.h"
#include "droid_engine.h"
#include "droid_logger.h"
#include "droid_thread_task.h"

using namespace Droid;

WorkerManager::WorkerManager() 
	: System("WorkerManager") {}

WorkerManager::~WorkerManager() 
{
	DestroyWorkers(-1);
}

System* WorkerManager::Detach(System* _system)
{
	DestroyWorkers();
	
	return System::Detach(_system);	
}

void WorkerManager::Update(double _dt)
{
	// transfer outgoing messages to workers
	Array<MsgQueue_Msg>* m_msgOutgoingArray = m_msqOutgoing.GetMessages();

	unsigned int i=0,workerCounter=0,_numPushed = 0;
	for( i; i < m_msgOutgoingArray->Size(); i++ )
	{
		MsgQueue_Msg _msg = (*m_msgOutgoingArray)[i];
		Worker* _w = 0;

		// find free worker that we already haven't looked at
		for( unsigned int j=workerCounter; j < m_workers.Size(); j++)
		{
			if ( m_workers[j]->IsFree() )
			{
				_w = m_workers[j];
				break;
			}
		}

		if ( _w )
		{
			_w->Msq()->Push( _msg.msg, _msg.data, _msg.datalength, 0, false );
			_msg.msg        = "";
			_msg.data       = 0;
			_msg.datalength = 0;

			_numPushed++;
			workerCounter++;
		} else {
			break;
		}
	}

	// if we have pushed buffered messages, move rest of messages to front
	if ( _numPushed > 0 )
	{
		for( unsigned int i=_numPushed,_start=0; i < m_msgOutgoingArray->Size(); i++,_start++ )
		{
			(*m_msgOutgoingArray)[_start] = (*m_msgOutgoingArray)[i];
		}

		m_msgOutgoingArray->Resize( m_msgOutgoingArray->Size() - _numPushed );
	}

	Logger* logger = (Logger*) Engine::Get()->GetSystem("Logger");

	// poll all messages from workers 
	MsgQueue_Msg _msqMsg = m_msq.PopNonBlocking();

	while( _msqMsg.msg != 0 )
	{
		const char* _msg     = _msqMsg.msg; 
		unsigned int _sender = _msqMsg.sender;
		void* _data          = _msqMsg.data;
		unsigned int _length = _msqMsg.datalength;

		if ( strcmp(_msg,"MSG_HELLO") == 0 )
		{
			logger->Log(DROID_LOG_DEBUG,"WorkerManager","Worker %u says hello", _sender); 
		}
		else if ( strcmp(_msg,"MSG_LOG_INFO") == 0 )
		{
			char* _log = (char*) _data;

			char _buffer[32];
			sprintf_s(_buffer,32,"Worker[%u]", _sender);
			size_t _bufferlen   = strlen(_buffer);
			_buffer[_bufferlen] = 0;
			
			logger->Log(DROID_LOG_INFO,_buffer,"%s", _log);

			// clean up data pointer
			DROID_FREE_PTR(_data);
		}
		else if ( strcmp(_msg, "MSG_TASK_COMPLETE") == 0 )
		{
			char _buffer[32];
			sprintf_s(_buffer,32,"Worker[%u]", _sender);
			size_t _bufferlen   = strlen(_buffer);
			_buffer[_bufferlen] = 0;

			logger->Log(DROID_LOG_INFO,_buffer,"Finished task %s", (char*) _data );
		}

		_msqMsg = m_msq.PopNonBlocking();
	}

	System::Update(_dt);
}

void WorkerManager::Send(const char* _msg, void* _data, unsigned int _datalength, bool _copy)
{
	// find free worker
	Worker* w = 0;
	for(unsigned int i=0; i < m_workers.Size(); i++)
	{
		if ( m_workers[i] && m_workers[i]->IsFree() )
		{
			w = m_workers[i];
			break;
		}
	}

	if(!w || m_workers.Size() == 0)
	{
		// if no worker is free, push to outgoing message queue
		m_msqOutgoing.Push( _msg, _data, _datalength, 0, _copy );
	}
	else
	{
		// push directly to worker
		w->Msq()->Push( _msg, _data, _datalength, 0, _copy );
	}
} 

void WorkerManager::CreateWorkers(unsigned int _count)
{
	// spawn workers
	for(unsigned int i=0; i < _count; i++)
	{
		m_workers.Push(new Worker(&m_msq, m_workers.Size() + 1));
	}

	// send hello msg to all new workers (mainly for debugging)
	for(unsigned int i=0; i < _count; i++)
	{
		m_workers[i]->Msq()->Push("MSG_HELLO", 0);
	}
}

void WorkerManager::DestroyWorkers(int _count)
{
	// destroying 0 workers will erase all current workers
	if ( _count < 1 )
		 _count = (int) m_workers.Size();

	for(int i=0; i < _count; i++)
	{	
		// deleting workers will join the threads
		m_workers[i]->Msq()->Push("MSG_SHUTDOWN", 0);
		DROID_DEL_PTR(m_workers[i]);
	}

	m_workers.Resize(_count);
}

