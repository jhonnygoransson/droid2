#include "droid_worker.h"
#include "droid_thread_task.h"

#define DROID_WORKER_LOG(...)	{ \
		char _tmp[260]; \
		sprintf_s( _tmp, 256, __VA_ARGS__ ); \
		size_t _tmplen = strlen(_tmp); \
		_tmp[_tmplen]  = 0x0; \
		m_managerMsq->Push( "MSG_LOG_INFO", _tmp, _tmplen+1, m_identifier ); \
	} 

using namespace Droid;

static void WorkerRunWrapper(void *_this) { ((Worker*)_this)->Run(); };

Worker::Worker()
	: m_managerMsq(0)
	, m_identifier(0)
{
	m_thread = new tthread::thread( (WorkerRunWrapper), (void*)this );
}

Worker::Worker(MsgQueue* _managerMsq, unsigned int _identifier)
	: m_managerMsq(_managerMsq)
	, m_identifier(_identifier)
{
	assert(_managerMsq);
	m_thread = new tthread::thread( (WorkerRunWrapper), (void*)this );
}

Worker::~Worker()
{
	if(m_thread)
		m_thread->join();

	DROID_DEL_PTR(m_thread);
}

void Worker::Run()
{
	bool _running = true;

	while(_running)
	{
		MsgQueue_Msg _msqMsg = m_msq.Pop();
		const char* _msg     = _msqMsg.msg;

		// flag the worker that we have work to do
		m_free = false;

		if ( strcmp(_msg,"MSG_HELLO") == 0 )
		{
			m_managerMsq->Push( "MSG_HELLO", m_identifier );
		}
		else if ( strcmp(_msg,"MSG_SHUTDOWN") == 0 )
		{
			_running = false;
		} 
		else if ( strcmp(_msg, "MSG_TASK") == 0 )
		{
			ThreadTask* _task = (ThreadTask*) _msqMsg.data;

			if(_task->Run())
			{
				m_managerMsq->Push( "MSG_TASK_COMPLETE", (void*) _task->GetName(), strlen(_task->GetName()), m_identifier, false );
			}
			else DROID_WORKER_LOG("Task '%s' Failed", _task->GetName());
		}
		else 
		{
			DROID_WORKER_LOG( "Unknown msg received : %s", _msg );
		}

		m_free = true;
	}
}