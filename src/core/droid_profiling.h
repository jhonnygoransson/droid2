#pragma once

// just a wrapper for brofiler
#ifdef DROID_PROFILING
	#include "Brofiler.h"

	#define DROID_PF_EVENT         BROFILER_EVENT
	#define DROID_PF_PROFILE       PROFILE
	#define DROID_PF_INLINE_EVENT  BROFILER_INLINE_EVENT
	#define DROID_PF_CATEGORY      BROFILER_CATEGORY
	#define DROID_PF_FRAME         BROFILER_FRAME
	#define DROID_PF_THREAD        BROFILER_THREAD
#else
	#define DROID_PF_EVENT
	#define DROID_PF_PROFILE
	#define DROID_PF_INLINE_EVENT
	#define DROID_PF_CATEGORY
	#define DROID_PF_FRAME(...) 
	#define DROID_PF_THREAD
#endif