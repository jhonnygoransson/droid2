#pragma once

#include "tinythread.h"
#include "droid_common.h"

namespace Droid
{
	typedef struct 
	{
		const char*  msg;
		void*        data;
		unsigned int datalength;
		unsigned int sender;
	} MsgQueue_Msg;

	class MsgQueue 
	{	
	public:
		MsgQueue();
		~MsgQueue();

		void                 Push(const char* _msg, void* _data, unsigned int _datalength, unsigned int _sender, bool _copy = true);
		void                 Push(const char* _msg, unsigned int _sender);
		MsgQueue_Msg         Pop();
		MsgQueue_Msg         PopNonBlocking();
		unsigned int         Size();
		MsgQueue_Msg*        GetMsg(unsigned int _index);
		Array<MsgQueue_Msg>* GetMessages();

	private:
		Array<MsgQueue_Msg>         m_messages;
		tthread::mutex              m_mutex;
		tthread::condition_variable m_cond;
	};
}