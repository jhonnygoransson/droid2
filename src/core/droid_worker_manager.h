#pragma once

#include "droid_system.h"
#include "droid_common.h"
#include "droid_worker.h"
#include "droid_msg_queue.h"

namespace Droid
{
	class WorkerManager : public System
	{
	public:
		WorkerManager();
		~WorkerManager();

		void CreateWorkers(unsigned int _count);
		void DestroyWorkers(int _count = -1);

		System* Detach(System* _system);
		void Update(double _dt);

		void Send( const char* _msg, void* _data = 0, unsigned int _datalength = 0, bool _copy = true);

	private:
		Array<Worker*> m_workers;
		MsgQueue m_msq;
		MsgQueue m_msqOutgoing;
	};	
}