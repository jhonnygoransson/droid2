#pragma once

#include "droid_common.h"
#include "droid_event.h"

namespace Droid
{
	class System;
	class System 
	: public Publisher
	, public Subscriber
	{
	public:
		System();
		System(const char* _name);
		virtual ~System();

		virtual System* Attach(System* _system);
		virtual System* Detach(System* _system);
		virtual System* DetachAll();

		virtual System* GetSystem(const char* _name);
		virtual System* GetSystem(unsigned int _index);
		virtual unsigned int GetSystemCount();

		virtual void Update(double _dt);

		// notify all sub-systems that some event has occured
		// todo: userdata / better message system...
		virtual void Notify(const char* _event);
		virtual void SubscribeTo(const char* _event);
		virtual bool OnEvent(const char* _event);

	protected:
		virtual bool SubscribesTo(const char* _event);

		const char* m_name;
		System* m_parent;

	private:
		Array<System*> m_systems;
		Array<const char*> m_subscriptions;
	};
}