#include <assert.h>
#include <stdlib.h>

#include "droid_engine.h"
#include "droid_configurator.h"
#include "droid_logger.h"
#include "droid_function.h"
#include "script/droid_script_system.h"
#include "file_system/droid_path.h"
#include "file_system/droid_file_disk.h"

namespace Droid
{
	Variable Configurator::ConfigValueNotFound;

	struct ConfigItem
	{
		const char* key;
		Variable value;
	};
	
	class ConfiguratorImpl : public Configurator
	{
	public:
		ConfiguratorImpl()
		: m_configPathRaw("config.lua")
		{
			SubscribeTo("RegisterScriptAPI");
		}

		~ConfiguratorImpl() 
		{
		}

		bool OnEvent(const char* _event)
		{
			if ( strcmp(_event,"RegisterScriptAPI") == 0 )
			{
				RegisterScriptAPI();
			}
			else if ( strcmp(_event,"EngineCreated") == 0 )
			{
				ReadConfig();
			}
  
			return true;
		}

		void RegisterScriptAPI()
		{
			Engine::Get()->GetSystem<Logger>("Logger")->Log( DROID_LOG_DEBUG,m_name,"Registering Script API functions");
			ScriptSystem* _sc = Engine::Get()->GetSystem<ScriptSystem>("ScriptSystem");

			#define REGISTER_FUNCTION(FUNC,FUNCNAME) \
			{ \
				static Function FUNC##Wrapper = \
				Function::Build<void(ConfiguratorImpl::*)(const char*, const char*),&ConfiguratorImpl::FUNC>(&ConfiguratorImpl::FUNC); \
				FUNC##Wrapper.Bind( this ); \
				_sc->RegisterLuaFunction( #FUNCNAME, &FUNC##Wrapper ); \
			}

			REGISTER_FUNCTION(LuaConfigure,configure);
							
			#undef REGISTER_FUNCTION
		}

		void ReadConfig()
		{
			// load script file
			ScriptSystem* _sc = Engine::Get()->GetSystem<ScriptSystem>("ScriptSystem");
			Path          _scriptPath(m_configPathRaw);
			FileDisk      _scriptFile(_scriptPath);
			Script*       _configScript = _sc->Load(_scriptFile);
		}

		void SetConfigPath(const char* _path)
		{
			m_configPathRaw = _path;
		}

		void LuaConfigure(const char* _key, const char* _value)
		{
			SetConfigValue(_key, _value);
		}

		void SetConfigValue(const char* _key, const char* _value)
		{
			TypeInfo* cCharTypeInfo = Introspection::GetType<const char*>();
			Variable _valueVariable(cCharTypeInfo);
			_valueVariable.SetValue((void*)_value);

			SetConfigValue(_key,_valueVariable);
		}

		void SetConfigValue(const char* _key, const Variable& _variable)
		{
			ConfigItem* _item = GetConfigValue(_key);
			Variable _configVariable(_variable);

			if ( _item )
			{
				_item->value = _configVariable;
			}
			else
			{
				ConfigItem _newItem;
				_newItem.key   = _key; 
				_newItem.value = _configVariable;

				m_configArray.Push(_newItem);
			}
		}

		ConfigItem* GetConfigValue( const char* _key )
		{
			for( unsigned int i = 0; i < m_configArray.Size(); i++ )
			{
				if ( strcmp(m_configArray[i].key, _key) == 0 )
					return &m_configArray[i];
			}

			return 0;
		}

		Variable& GetConfigAsVariable( const char* _key )
		{
			ConfigItem* configValue = GetConfigValue(_key);

			if ( configValue )
			{
				return configValue->value;
			}

			return ConfigValueNotFound;
		}

		const char* GetConfigAsString( const char* _key, const char* _default )
		{
			ConfigItem* _item = GetConfigValue(_key);

			if ( _item ) 
			{
				return _item->value.GetValue<const char*>();
			}

			return _default;
		}

		int GetConfigAsInt( const char* _key, int _default )
		{
			ConfigItem* _item = GetConfigValue(_key);
			
			if ( _item )
			{
				return _item->value.GetValue<int>();
			}

			return _default;
		}

		float GetConfigAsFloat( const char* _key, float _default )
		{
			ConfigItem* _item = GetConfigValue(_key);
			
			if ( _item ) 
			{
				return _item->value.GetValue<float>();
			}

			return _default;
		}

		bool GetConfigAsBool( const char* _key, bool _default )
		{
			ConfigItem* _item = GetConfigValue(_key);
			
			if ( _item )
			{
				return _item->value.GetValue<bool>();
			}

			return _default;
		}


	private:
		const char* m_configPathRaw;
		Array<ConfigItem> m_configArray;
	};

	Configurator* Configurator::Create()
	{
		return new ConfiguratorImpl();
	}
}