#include "droid_msg_queue.h"

using namespace Droid;

MsgQueue::MsgQueue() {}
MsgQueue::~MsgQueue() {}

void MsgQueue::Push(const char* _msg, void* _data, unsigned int _datalength, unsigned int _sender, bool _copy)
{
	m_mutex.lock();

	MsgQueue_Msg _msq_msg;

	_msq_msg.msg    = _msg;
	_msq_msg.sender = _sender;

	if ( _copy ) // if we should copy message, do so
	{
		if ( _data && _datalength > 0 )
		{
			_msq_msg.data       = (void*) malloc( _datalength );
			_msq_msg.datalength = _datalength;
			memcpy(_msq_msg.data, _data, _datalength);
		} else {
			_msq_msg.data       = 0;
			_msq_msg.datalength = 0;
		}
	} 
	else // otherwise, transfer pointers directly
	{
		_msq_msg.data = _data;
		_msq_msg.datalength = _datalength;
	}

	m_messages.Push(_msq_msg);

	m_cond.notify_one();
	m_mutex.unlock();
}

Array<MsgQueue_Msg>* MsgQueue::GetMessages()
{
	tthread::lock_guard<tthread::mutex> guard(m_mutex);
	return &m_messages;
}

MsgQueue_Msg* MsgQueue::GetMsg(unsigned int _index)
{
	MsgQueue_Msg* _msg = 0;

	tthread::lock_guard<tthread::mutex> guard(m_mutex);

	if ( _index < m_messages.Size() )
	{
		return &m_messages[_index];
	}

	return 0;
}

void MsgQueue::Push(const char* _msg, unsigned int _sender)
{
	Push(_msg,0,0,_sender);
}

MsgQueue_Msg MsgQueue::Pop()
{
	m_mutex.lock();

	while( m_messages.Size() == 0 )
		m_cond.wait(m_mutex);

	MsgQueue_Msg _msg = m_messages.Pop();
	
	m_mutex.unlock();

	return _msg;
}

MsgQueue_Msg MsgQueue::PopNonBlocking()
{
	MsgQueue_Msg _msg;
	_msg.msg        = 0;
	_msg.data       = 0;
	_msg.datalength = 0;

	if (m_mutex.try_lock())
	{
		if( m_messages.Size() > 0 )
		{
			_msg = m_messages.Pop();
		}

		m_mutex.unlock();
	}

	return _msg;
}

unsigned int MsgQueue::Size()
{
	unsigned int res;
	m_mutex.lock();
	res = m_messages.Size();
	m_mutex.unlock();

	return res;
}