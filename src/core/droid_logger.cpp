#include "droid_logger.h"
#include "droid_engine.h"
#include "droid_function.h"

#include "script/droid_script_system.h"

#include <stdarg.h>
#include <malloc.h>

#include <stdio.h>
#include <string.h>

using namespace Droid;

Logger::Logger()
    : System("Logger") 
{
    SubscribeTo("RegisterScriptAPI");
}
    
Logger::~Logger() {}

// TODO: remove malloc here and use fixed size buffer instead
void Logger::Log(char _level, const char* _scope, const char* _message, ...)
{
	char* buffer;
	int len;

    va_list va;
    va_start(va, _message);

	len 	= _vscprintf( _message, va ) + 1;
	buffer 	= (char*) malloc( len * sizeof( char) );

    vsprintf_s(buffer, len, _message, va);
    va_end(va);

    printf("Droid['%s'] :: %s\n",_scope,buffer);

    /*
    switch( _level )
    {
        case(DROID_LOG_LEVEL_INFO):
            printf("[INFO]: %-*s :: %s\n",DROID_MAX_SCOPE_LEN,_scope,buffer);
            break;
        case(DROID_LOG_LEVEL_ERROR):
            printf("[ ERR]: %-*s :: %s\n",DROID_MAX_SCOPE_LEN,_scope,buffer);
            break;
        case(DROID_LOG_LEVEL_DEBUG):
            printf("[ DBG]: %-*s :: %s\n",DROID_MAX_SCOPE_LEN,_scope,buffer);
            break;
        case(DROID_LOG_LEVEL_SPAM):
            #if DROID_ALLOW_SPAM > 0
                printf("[ SPM]: %-*s :: %s\n",DROID_MAX_SCOPE_LEN,_scope,buffer);
            #endif
            break;
    }
    */

    free(buffer);
}

void Logger::LuaLog()
{
    // todo: arguments unwrapping so that we can pass to 'real' log function
}

bool Logger::OnEvent(const char* _event)
{
    if ( strcmp(_event,"RegisterScriptAPI") == 0 )
    {
        Log(DROID_LOG_DEBUG,m_name,"Registering Script API functions");

        ScriptSystem* _sc = static_cast<ScriptSystem*>(Engine::Get()->GetSystem("ScriptSystem"));

        // generate a class function wrapper for our lua wrapper method
        static Droid::Function _logFunction = Droid::Function::Build<void(Logger::*)(void),&Logger::LuaLog>(&Logger::LuaLog);

        // bind functino context to this instance
        _logFunction.Bind( this );

        // finally, register lua function with 
        _sc->RegisterLuaFunction( "log", &_logFunction );
    }

	return true;
}