#include "droid_system.h"

using namespace Droid;

System::System() 
	: m_name("Unknown System")
	, m_parent(0)
{}

System::System(const char* _name) 
	: m_name(_name)
	, m_parent(0) 
{}

System::~System() 
{

	DetachAll();

	m_systems.Reset();
	m_subscriptions.Reset();
}

System* System::Detach(System* _system)
{
	int _systemIndex = m_systems.IndexOf(_system);

	if ( _systemIndex >= 0 )
		m_systems[_systemIndex] = 0;
	
	if(_system->SubscribesTo("Detached"))
		_system->OnEvent("Detached");

	return this;
}

System* System::DetachAll()
{
	while(m_systems.Size() > 0)
	{
		System* _systemToDetach = m_systems.Pop();
		Detach( _systemToDetach );
	}

	return this;
}

unsigned int System::GetSystemCount()
{
	return m_systems.Size();
}

System* System::GetSystem(unsigned int _index)
{
	if ( _index < 0 || _index >= m_systems.Size() ) return 0;

	return m_systems[_index];
}

System* System::GetSystem(const char* _name)
{
	for( unsigned int i = 0; i < m_systems.Size(); i++ )
	{
		if ( m_systems[i] )
		{
			if ( strcmp( _name, m_systems[i]->m_name ) == 0 )
				return m_systems[i];
			else m_systems[i]->GetSystem(_name);
		}
	}

	return 0;
}

void System::Update(double _dt)
{
	for( unsigned int i = 0; i < m_systems.Size(); i++ )
	{
		if ( m_systems[i] )
		{
			m_systems[i]->Update(_dt);
		}
	}
}

System* System::Attach(System* _system)
{
	assert(_system);
	assert(m_systems.IndexOf(_system) < 0);

	_system->m_parent = this;
	m_systems.Push(_system);

	if(_system->SubscribesTo("Attached"))
		_system->OnEvent("Attached");

	return this;
}

void System::Notify(const char* _event)
{
	// first, check if this system subscribes to this event
	if ( SubscribesTo(_event) )
	{
		// call the event function
		if ( OnEvent(_event) )
		{
			// if it returns true, bubble down the event to all sub-systems
			for( unsigned int i = 0; i < m_systems.Size(); i++ )
			{	
				/*
				if ( m_systems[i] && m_systems[i]->SubscribesTo(_event) )
				{
					if( m_systems[i]->OnEvent(_event))
						m_systems[i]->Notify(_event);
				}
				*/

				m_systems[i]->Notify(_event);
			}
		}
	}
}

void System::SubscribeTo(const char* _event)
{
	if( m_subscriptions.IndexOf(_event) >= 0 ) return;

	m_subscriptions.Push(_event);
}

bool System::OnEvent(const char* _event)
{
	return true;
}

bool System::SubscribesTo(const char* _event)
{
	return m_subscriptions.IndexOf(_event) >= 0;
}