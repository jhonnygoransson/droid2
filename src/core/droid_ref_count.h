#pragma once

namespace Droid
{
	class RefCount
	{
	public:
		RefCount();
		~RefCount();

		void IncRef();
		void DecRef();

	protected:
		unsigned int m_refCount;
	};
}