#pragma once

#include "droid_system.h"

namespace Droid
{
	class Logger : public System
	{
	public:
		Logger();
		~Logger();

		bool OnEvent(const char* _event);

		void Log(char _level, const char* _scope, const char* _message, ...);
		void LuaLog();
	private:
		// todo: log to file etc
	};
}