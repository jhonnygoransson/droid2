#include "droid_introspection.h"

namespace Droid
{
	class IntrospectionImpl : public Introspection
	{
	public:
		IntrospectionImpl()	
		{
			SubscribeTo("Attached");
		}

		~IntrospectionImpl() {}

		void AddType( TypeInfo* _typeInfo )
		{
			m_typeMap.Push( _typeInfo );
		}

		bool OnEvent(const char* _event)
		{
			if ( strcmp(_event,"Attached") == 0 )
			{
				RegisterDefaultTypes();
			}

			return true;
		}

		void RegisterDefaultTypes()
		{
			#define REGISTER_POD(t) \
				Droid::Introspection::RegisterType<t>( sizeof(t), #t );
			
			REGISTER_POD(int);
			REGISTER_POD(const char*);
			REGISTER_POD(float);
			REGISTER_POD(bool);

			#undef REGISTER_POD

			Notify("DefaultTypesRegistered");
		}

	private:

		Array<TypeInfo*> m_typeMap;
	};

	Introspection* Introspection::Create()
	{
		return new IntrospectionImpl();
	}
}