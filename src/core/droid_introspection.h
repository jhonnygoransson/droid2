#pragma once

#include "droid_interfaces.h"
#include "droid_system.h"
#include "droid_engine.h"

namespace Droid
{
	template <typename T> 
	void TypeInfo::PlacementCopy( void* _data, void* _src )
	{
		new (_data) T( *((T) _src));
	}

	class Introspection : public System
	{
	public:
		Introspection() : System("Introspection") {}
		virtual ~Introspection() {}

		virtual void AddType( TypeInfo* _typeInfo ) = 0;

		template <typename T>
		static TypeInfo* GetType()
		{
			static TypeInfo instance;
			return &instance;
		}

		template <typename T>
		static void RegisterType( unsigned _size, const char* _typeName )
		{
			TypeInfo* _typeInfo = GetType<T>();

			_typeInfo->typeName = _typeName;
			_typeInfo->size     = _size;

			Introspection* _introspection = static_cast<Introspection*>(Droid::Engine::Get()->GetSystem("Introspection"));
			assert(_introspection);

			_introspection->AddType( _typeInfo );
		}

		static Introspection* Create();
	};
}