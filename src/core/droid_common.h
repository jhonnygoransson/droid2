#pragma once

#include <malloc.h>
#include <assert.h>
#include <string.h>

#define DROID_FREE_PTR(p)\
    if(p) { \
        free(p); \
        p = 0; \
    }

#define DROID_DEL_PTR(p)\
    if(p) { \
        delete p; \
        p = 0; \
    }

#define DROID_DEL_ARR(p)\
    if(p) { \
        delete [] p; \
        p = 0; \
    }

#define DROID_LOG_DEBUG 0
#define DROID_LOG_INFO  1
#define DROID_LOG_WARN  2
#define DROID_LOG_ERR   3 

#define DROID_MAX_PATH_SIZE 260 // 256 + null-terminated and aligned block

namespace Droid
{
	template<class T>
	class Array
	{
	public:
		Array();
		~Array();

		T& operator [] (unsigned int _index);

		void Reset();
		unsigned int Size();

		void Resize(unsigned int _size);
		void Reverse();

		int IndexOf(T _item);

		void Push(T _item);
		T Pop();

	private:
		void _Init();
		void _AllocateCapacity(unsigned int _size);

		T* m_data;
		unsigned int m_size;
		unsigned int m_byteSize;
		unsigned int m_capacity;

		const static int m_arrayInitialSize    = 1;
		const static int m_arrayStepMultiplier = 2;
	};

	template<class T>
	class Cache
	{
	public:
	private:
	};

	#include "droid_array.inl"
}