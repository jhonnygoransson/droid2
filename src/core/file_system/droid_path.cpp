#include <assert.h>
#include <string.h>

#include "droid_path.h"

using namespace Droid;

Path::Path() { *this = ""; }
Path::Path(const char* _path) { *this = _path; }
Path::~Path() {}

void Path::operator=(const Path& _rhs)
{
	*this = _rhs.m_path;
}

void Path::operator=(const char* _rhs)
{
	// bounds check
	size_t _rhsSize = strlen(_rhs);
	assert( _rhsSize < DROID_MAX_PATH_SIZE - 4 ); // actual max size is 256

	// copy data
	memcpy ( (void*) m_path, (void*) _rhs, _rhsSize );

	// null terminate
	m_path[_rhsSize] = '\0';
	m_size = _rhsSize;
}

bool Path::operator==(const Path& _rhs) const
{
	return strcmp(m_path,_rhs.m_path) == 0;
}

char* Path::Get()
{
	return m_path;
}

size_t Path::Size()
{
	return m_size;
}