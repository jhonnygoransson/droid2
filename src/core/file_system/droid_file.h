#pragma once

#include "droid_file_system.h"
#include "droid_path.h"

namespace Droid
{
	class File
	{
	public:
		File() 
			: m_pos(0)
			, m_size(0)
			, m_mode(FileSystem::NONE)
			, m_path()
		{}

		File(const Path& _path) 
			: m_pos(0)
			, m_size(0)
			, m_mode(FileSystem::NONE)
			, m_path(_path)
		{}

		virtual ~File() {}

		enum SeekMode
		{
			BEGIN   = 0x0,
			CURRENT = 0x1,
			END     = 0x2
		};

		virtual bool Open( const Path& _path, FileSystem::Mode _mode ) = 0;
		virtual bool Open( FileSystem::Mode _mode ) = 0;

		virtual inline void Close() 
		{
			m_mode = FileSystem::NONE;
			m_pos  = 0;
		}

		virtual bool Read(char* _bytes, size_t _numBytes) = 0;
		virtual bool Write( char* _bytes, size_t _numBytes ) = 0;

		virtual bool Read(char* _bytes)
		{
			// read until end from current pos
			return Read(_bytes,m_size - m_pos);
		}

		virtual inline void Rewind() 
		{ 
			m_pos = 0; 
		}

		virtual inline size_t Pos() 
		{ 
			return m_pos; 
		}

		virtual inline size_t Size()
		{ 
			return m_size; 
		}

		virtual inline size_t Seek( SeekMode _base, size_t _offset ) 
		{  
			switch(_base)
			{
				case(BEGIN): m_pos = _offset; break;
				case(CURRENT): m_pos += _offset; break;
				case(END): m_pos = m_size + _offset; break;
				default: break;
			}

			if ( m_pos >= 0 && m_pos < m_size ) return 0;
			return -1;
		}

		virtual inline Path& GetPath() 
		{
			return m_path;
		}

		virtual inline FileSystem::Mode GetMode()
		{
			return m_mode;
		}

	protected:
		size_t           m_pos;
		unsigned int     m_size;
		FileSystem::Mode m_mode;
		Path       		 m_path;
	};
}