#include <assert.h>

#include "droid.h"
#include "droid_file_system.h"
#include "droid_file_disk.h"
#include "droid_file_memory.h"
#include "droid_worker_manager.h"

class ThreadTask
	{
	public:
		ThreadTask() {}
		virtual ~ThreadTask() {}
		virtual bool Run() = 0;
		virtual const char* GetName() = 0;
	};

namespace Droid
{
	class AsyncFileLoadingTask : public ThreadTask
	{
	public:
		AsyncFileLoadingTask( File* _file, const char* _path, FileSystem::Mode _mode ) 
			: m_file(_file)
			, m_path()
			, m_mode(_mode)
		{
			m_path = _path;
		}

		~AsyncFileLoadingTask() {}

		bool Run() 
		{
			if(!m_file->Open(m_path,m_mode))
			{
				DROID_DEL_PTR(m_file);
				return false;
			}

			size_t _byteSize = sizeof(char) * m_file->Size();
			char* _bytes     = (char*) malloc( _byteSize );

			m_file->Read( _bytes, _byteSize );

			// TODO: callbacks
			DROID_DEL_PTR(m_file);
			DROID_FREE_PTR(_bytes);

			return true;
		}

		const char* GetName() { return "AsyncFileLoadingTask"; }

	private:
		File*            m_file;
		Path             m_path;
		FileSystem::Mode m_mode;
	};
}

using namespace Droid;

FileSystem::FileSystem()
	: System("FileSystem")
{}

FileSystem::~FileSystem()
{}

File* FileSystem::GetFileByLocation(Location _location)
{
	switch( _location )
	{
		case(MEMORY): 
			return new FileMemory();
		case(DISK): 
			return new FileDisk();  
		case(ZIP):    
		case(NETWORK):
		default:
			return 0;
	}
	return 0;
}

File* FileSystem::Open( Location _location, const char* _path, Mode _mode )
{
	File* _newFile = GetFileByLocation(_location);

	if (!_newFile) return 0;

	Path _droidPath(_path);

	if ( !_newFile->Open( _droidPath, _mode ) )
	{
		DROID_DEL_PTR(_newFile);
		return 0;
	}

	return _newFile;
}

void FileSystem::OpenAsync( Location _location, const char* _path, Mode _mode )
{
	WorkerManager* _wm = (WorkerManager*) Droid::Engine::Get()->GetSystem("WorkerManager");
	File* _newFile     = GetFileByLocation(_location);

	if(!_newFile) return;

	AsyncFileLoadingTask* _task = new AsyncFileLoadingTask(_newFile, _path, _mode);

	_wm->Send("MSG_TASK", (void*) _task, sizeof(AsyncFileLoadingTask));
}

void FileSystem::Close( File* _file )
{
	assert(_file);
	_file->Close();
}
