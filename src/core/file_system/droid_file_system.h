#pragma once

#include "core/droid_system.h"

namespace Droid
{
	class File;

	class FileSystem : public System
	{
	public:
		FileSystem();
		~FileSystem();

		enum Mode
		{
			NONE   = 0x0,
			READ   = 0x1,
			WRITE  = 0x2,
			OPEN   = 0x4,
			CREATE = 0x8
		};

		enum Location
		{
			MEMORY  = 0x0,
			DISK    = 0x1,
			ZIP     = 0x2,
			NETWORK = 0x4
		};

		File* Open( Location _location, const char* _path, Mode _mode );
		void  OpenAsync( Location _location, const char* _path, Mode _mode );
		void  Close( File* _file );

	private:
		File* GetFileByLocation(Location _location);
	};

	inline FileSystem::Mode operator|(FileSystem::Mode a, FileSystem::Mode b) {
		return static_cast<FileSystem::Mode>(static_cast<int>(a) | static_cast<int>(b));
	}
}