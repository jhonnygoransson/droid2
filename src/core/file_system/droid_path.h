#pragma once

#include "core/droid_common.h"

namespace Droid
{
	class Path
	{
	public:
		Path();
		Path(const char* _path);
		~Path();

		void operator=(const Path& _rhs);
		void operator=(const char* _rhs);
		bool operator==(const Path& _rhs) const;

		char* Get();
		size_t Size();

		/* todo:
		void Normalize();
		char* GetAbsolute();
		char* GetExt();
		char* GetBase();
		char* GetBaseDir();
		bool HasExtension();
		*/

	private:
		char  m_path[DROID_MAX_PATH_SIZE];
		size_t m_size;
	};
}