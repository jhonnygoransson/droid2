#pragma once

#include "droid_file.h"

namespace Droid
{
	class FileMemory : public File
	{
	public:

		FileMemory();
		~FileMemory();

		bool Open( FileSystem::Mode _mode );
		bool Open( const Path& _path, FileSystem::Mode _mode );
		void Close();

		bool Read(char* _bytes, size_t _numBytes);
		bool Write(char* _bytes, size_t _numBytes);

	private:
		char* m_buffer;
	};
}