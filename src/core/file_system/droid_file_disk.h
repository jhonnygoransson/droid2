#include "droid_file.h"

namespace Droid
{
	class FileDisk : public File
	{
	public:

		FileDisk();
		FileDisk(const Path& _path);

		~FileDisk();

		bool   Open( FileSystem::Mode _mode );
		bool   Open( const Path& _path, FileSystem::Mode _mode );
		void   Close();
		bool   Read(char* _bytes, size_t _numBytes);
		bool   Write(char* _bytes, size_t _numBytes);
		size_t Seek(SeekMode _base, size_t _offset );

	private:
		struct FileDiskImpl* m_impl;
	};
}