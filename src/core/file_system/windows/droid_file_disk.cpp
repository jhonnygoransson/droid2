#include <windows.h>
#include <assert.h>

#include "droid_file_disk.h"
#include "droid_file_system.h"
#include "util/windows/droid_platform_utils_windows.h"

namespace Droid
{
	struct FileDiskImpl
	{
		HANDLE handle;
	};
}

using namespace Droid;

FileDisk::FileDisk() 
	: m_impl(0)
{}

FileDisk::FileDisk(const Path& _path)
	: File(_path)
	, m_impl(0)
{}

FileDisk::~FileDisk() 
{
	DROID_FREE_PTR(m_impl);
}

bool FileDisk::Open( FileSystem::Mode _mode )
{
	return Open(m_path, _mode);
}

bool FileDisk::Open( const Path& _path, FileSystem::Mode _mode ) 
{
	DWORD desiredAccessMode = 0;

	if ( FileSystem::WRITE & _mode )
		desiredAccessMode |= GENERIC_WRITE;

	if ( FileSystem::READ & _mode )
		desiredAccessMode |= GENERIC_READ;

	DWORD shareMode = FileSystem::WRITE & _mode ? 0 : FILE_SHARE_READ;
	DWORD creationDisposition = FileSystem::CREATE & _mode ? CREATE_ALWAYS : OPEN_EXISTING;

	m_path = _path;

	HANDLE hnd = ::CreateFile( 
		TEXT( m_path.Get() ),
		desiredAccessMode,
		shareMode,
		NULL,
		creationDisposition,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hnd != INVALID_HANDLE_VALUE )
	{
		if ( !m_impl )
		{
			m_impl = (FileDiskImpl*) malloc(sizeof(FileDiskImpl));
			m_impl->handle = NULL;
		}

		assert(m_impl);

		m_impl->handle = hnd;
		m_size = GetFileSize( m_impl->handle, 0);

		m_mode = _mode;
		m_path = _path;

		return true;
	}

	DROID_LAST_WINDOWS_ERROR(_errbuf);

	return false;
}

void FileDisk::Close() 
{	
	if ( m_impl )
	{
		::CloseHandle(m_impl->handle);
		m_impl->handle = 0;
	}

	File::Close();
}

bool FileDisk::Read(char* _bytes, size_t _numBytes)
{
	if ( !(m_mode & FileSystem::READ) )
		return false;

	assert(m_impl);

	size_t bytesRead = 0;
	::ReadFile( m_impl->handle, _bytes, (DWORD) _numBytes, (LPDWORD)&bytesRead, NULL);

	if(bytesRead != m_size)
	{
		DROID_LAST_WINDOWS_ERROR(_errbuf);
		return false;
	}

	size_t _currentpos = ::SetFilePointer(m_impl->handle, 0, NULL, FILE_CURRENT );
	File::Seek(File::CURRENT, _numBytes);
	assert( m_pos == _currentpos);

	return bytesRead == _numBytes;
}

bool FileDisk::Write(char* _bytes, size_t _numBytes) 
{
	if ( !(m_mode & FileSystem::WRITE) )
		return false;

	assert(_bytes);

	size_t bytesWritten = 0;
	::WriteFile(m_impl->handle, _bytes, (DWORD) _numBytes, (LPDWORD)&bytesWritten, NULL);

	size_t _currentpos = ::SetFilePointer(m_impl->handle, 0, NULL, FILE_CURRENT );
	File::Seek(File::CURRENT, _numBytes);
	assert( m_pos == _currentpos);

	return bytesWritten == _numBytes;
}

size_t FileDisk::Seek(SeekMode _base, size_t _offset )
{
	assert( m_impl );
	int _winbase = 0;

	switch(_base)
	{
		case File::BEGIN:   _winbase = FILE_BEGIN; break;
		case File::END:     _winbase = FILE_END; break; 
		case File::CURRENT: _winbase = FILE_CURRENT; break;
		default: return 0;
	}

	size_t _currentpos = ::SetFilePointer(m_impl->handle, (DWORD)_offset, NULL, _winbase);

	File::Seek(_base,_offset);
	
	assert( m_pos == _currentpos);

	return m_size;
}