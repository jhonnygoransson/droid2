#include <malloc.h>
#include <string.h>

#include "droid_file_memory.h"

using namespace Droid;

FileMemory::FileMemory() 
	: m_buffer(0)
{}

FileMemory::~FileMemory() {
	DROID_FREE_PTR(m_buffer);
}

bool FileMemory::Open( FileSystem::Mode _mode )
{
	return Open(m_path,_mode);
}

bool FileMemory::Open( const Path& _path, FileSystem::Mode _mode )
{
	if ( m_mode > 0 || _mode == m_mode ) return false;

	m_mode = _mode;
	m_path = _path;

	return true;
}

void FileMemory::Close()
{
	File::Close();
}

bool FileMemory::Read( char* _bytes, size_t _numBytes )
{
	assert(_bytes);

	if ( !(m_mode & FileSystem::READ) )
		return false;

	if ( m_size == 0 || (m_pos + _numBytes) > m_size )
	{
		return false;		
	}

	memcpy( _bytes, m_buffer + m_pos, _numBytes );

	m_pos += _numBytes;

	return true;
}

bool FileMemory::Write(char* _bytes, size_t _numBytes)
{
	if ( !(m_mode & FileSystem::WRITE) )
		return false;

	assert(_bytes);

	if ( m_buffer == 0 )
	{
		m_buffer = (char*) malloc( _numBytes * sizeof(char) );
		assert(m_buffer);
		m_size = _numBytes;
	}

	int v0          = _numBytes - m_pos;
	int v1          = m_size - m_pos;
	int bytesNeeded = v0 - v1;

	if ( bytesNeeded > 0 )
	{
		m_buffer = (char*) realloc( (void*) m_buffer, bytesNeeded );
		m_size  += bytesNeeded;

		assert(m_buffer);
	}

	memcpy( (m_buffer + m_pos), _bytes, _numBytes );

	m_pos += _numBytes;

	return true;
}