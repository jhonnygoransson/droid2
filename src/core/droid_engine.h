#pragma once

#include "util/droid_platform_context.h"
#include "droid_interfaces.h"

namespace Droid
{
	class Engine;
	class System;
	class Subscriber;

	class Engine
	{
	public:
		virtual ~Engine() {}

		static Engine*  Create(const PlatformContext ctx);
		static void     Destroy();
		static Engine*  Get();

		virtual void        Initialize() = 0;
		virtual System*     GetSystem(const char* _name) = 0;
		virtual void        Update(double _dt) = 0;
		virtual void        ConfigPath(const char* _path) = 0;
		virtual const       PlatformContext& GetPlatformContext() = 0;
		virtual EngineState GetEngineState() = 0;
		virtual void        PumpMessages() = 0;

		virtual void        AddSubscriber(Subscriber* subscriber) = 0;

		virtual System* Attach(System* _system) = 0;
		virtual System* Detach(System* _system) = 0;

		template <typename T>
		T* GetSystem(const char* _name)
		{
			return static_cast<T*>(GetSystem(_name));
		}

	protected:
		Engine() {}
	private:
		static Engine* m_engine;
	};
}