#pragma once

#include "core/droid_system.h"
#include "core/droid_function.h"
#include "core/droid_introspection.h"
#include "droid_script.h"

struct lua_State;

namespace Droid
{
	class File;

	class ScriptSystem : public System
	{
	public:
		typedef void (*LuaCallback)( lua_State*, int, Variable* );
		enum ConversionType { TO, FROM };

		ScriptSystem() : System("ScriptSystem") {}
		virtual ~ScriptSystem() {}

		virtual Script*      Load(File& _file) = 0;
		virtual lua_State*   GetState() = 0;
		virtual void         StackDump( lua_State* L, const char* _stateTag, const char* _header ) = 0;
		virtual void         RegisterLuaFunction( const char* _functionName, Function* _function ) = 0;
		virtual void         RegisterLuaTypeConverter( TypeInfo* _typeInfo, LuaCallback _luaCallback, ConversionType _conversionType ) = 0;

		template <typename T>
		static void RegisterLuaTypeConverterFrom( LuaCallback _luaCallbackFrom )
		{
			TypeInfo* _typeInfo = Introspection::GetType<T>();
			ScriptSystem* _sc   = static_cast<ScriptSystem*>(Droid::Engine::Get()->GetSystem("ScriptSystem"));

			_sc->RegisterLuaTypeConverter( _typeInfo, _luaCallbackFrom, FROM );
		}

		static ScriptSystem* Create();
	};
}