#include <string.h>
#include <new.h>

#include "droid_system.h"
#include "droid_script_system.h"
#include "droid_logger.h"
#include "droid_engine.h"
#include "droid_script.h"
#include "file_system/droid_file.h"
#include "droid_function.h"

#include "lua.hpp"

namespace Droid
{
	namespace ScriptSystemTypesConverter
	{
		static void LuaFromInt( lua_State* L, int index, Variable* variable )
		{
			assert( lua_isnumber( L, index ) );
    		int number = lua_tointeger(L, index);
			*(static_cast<int*>(variable->GetData())) = number;
		}

		static void LuaFromCharPtr( lua_State* L, int index, Variable* variable )
		{
    		assert( lua_isstring( L, index ) );
    		const char* str = lua_tostring(L, index);

    		// this should work, but doesn't
    		// *variable = str;

    		// ... so instead I do this, which is not as cool :(
    		variable->SetValue((void*)str);
		}

		static void LuaFromFloat( lua_State* L, int index, Variable* variable )
		{
			assert( lua_isnumber(L, index) );
			float number = (float) lua_tonumber(L, index);
			*(static_cast<float*>(variable->GetData())) = number;	
		}

		static void LuaFromBoolean( lua_State* L, int index, Variable* variable )
		{
			assert( lua_isboolean(L, index) );
			bool number = lua_toboolean(L, index) == 1 ? true : false;
			*(static_cast<bool*>(variable->GetData())) = number;
		}
	};

	class ScriptSystemImpl : public ScriptSystem
	{
	public:
		ScriptSystemImpl() 
		: m_libName("droid")
		{
			SubscribeTo("Attached");
			SubscribeTo("Detached");
			
			InitializeLua();
		}
		~ScriptSystemImpl() {
			lua_close(m_state);

			UnloadAllScripts();
		}

		void Update(double _dt)
		{
			System::Update(_dt);
		}

		void RegisterDefaulLuaConverters()
		{
			RegisterLuaTypeConverterFrom<int>( ScriptSystemTypesConverter::LuaFromInt );
			RegisterLuaTypeConverterFrom<char*>( ScriptSystemTypesConverter::LuaFromCharPtr );
			RegisterLuaTypeConverterFrom<float>( ScriptSystemTypesConverter::LuaFromFloat );
			RegisterLuaTypeConverterFrom<const char*>( ScriptSystemTypesConverter::LuaFromCharPtr );
			RegisterLuaTypeConverterFrom<bool>( ScriptSystemTypesConverter::LuaFromBoolean );
		}

		bool OnEvent( const char* _event )
		{
			if ( strcmp(_event,"Attached") == 0 )
			{
				m_parent->Notify("RegisterScriptAPI");

				// register default type converters
				RegisterDefaulLuaConverters();

				return false;
			}
			else if ( strcmp(_event,"Detached") == 0 )
			{
				m_parent->Notify("UnregisterScriptAPI");

				UnloadAllScripts();

				return false;
			}

			return true;
		}

		void InitializeLua()
		{
			m_state = luaL_newstate();

			if (m_state == NULL)
			{
				((Logger*) Engine::Get()->GetSystem("Logger"))->Log( DROID_LOG_ERR,m_name,"Could not allocate Lua state.");
				return;
			}

			static const luaL_Reg _core_lualibs[] = {
				{"", luaopen_base},
				{LUA_LOADLIBNAME, luaopen_package},
				{LUA_TABLIBNAME, luaopen_table},
				{LUA_OSLIBNAME, luaopen_os},
				{LUA_STRLIBNAME, luaopen_string},
				{LUA_MATHLIBNAME, luaopen_math},
				{LUA_DBLIBNAME, luaopen_debug},
				{NULL, NULL}
			};

			const luaL_Reg *lib = _core_lualibs;

			for (; lib->func; lib++) {
				lua_pushcfunction(m_state, lib->func);
				lua_pushstring(m_state, lib->name);
				lua_call(m_state, 1, 0);
			}

			// push 'droid' table
			lua_newtable(m_state);
			lua_setglobal(m_state,m_libName);
		}

		Script* Load(File& _file)
		{
			Script* _script = Script::Create();

			_script->Load(this, _file);

			m_scripts.Push(_script);

			return _script;
		}

		void UnloadAllScripts()
		{
			for( unsigned i=0; i < m_scripts.Size(); i++ )
			{
				Script* _script = dynamic_cast<Script*>(m_scripts[i]);
				DROID_FREE_PTR(_script);
			}

			m_scripts.Resize(0);
		}

		static int GenericLuaFunc( lua_State* L )
		{
			// extract function pointer from user data
			Function* _function    = static_cast<Function*>(lua_touserdata( L, lua_upvalueindex( 1 ) ));
			int       _numLuaArgs  = lua_gettop( L );
			unsigned  _numFnArgs   = _function->GetSignature()->GetArgCount();

			ScriptSystem* _sc = Engine::Get()->GetSystem<ScriptSystem>("ScriptSystem");
			ScriptSystemImpl* _scI = static_cast<ScriptSystemImpl*>(_sc);

			// dummy check argument count
			assert( _numLuaArgs == _numFnArgs );

			// TODO: return value
			Variable _returnValue;

			// allocate size for arguments on stack 
			Variable* _argumentStack = (Variable*) alloca( sizeof(Variable) * _numFnArgs );

			for( unsigned i = 0; i < _numFnArgs; i++ )
			{
				const TypeInfo* _argTypeInfo = _function->GetSignature()->GetArg(i);
				new (_argumentStack + i) Variable( _argTypeInfo, alloca(_argTypeInfo->size));
			}

			for( unsigned i = 0; i < _numFnArgs; i++ )
			{
				Variable* _varPtr = &_argumentStack[i];
				_scI->LuaConversionInternal( L, FROM, _varPtr, i + 1);
			}

			// call function
			(*_function)( _returnValue, _argumentStack, _numFnArgs );

			// assume nothing is returned
			return 0;
		}

		void LuaConversionInternal( lua_State* L, ConversionType _conversionType, Variable* &_variable, unsigned index )
		{
			if ( _conversionType == FROM )
			for( unsigned int i = 0; i < m_luaTypeConversionFrom.Size(); i++ )
			{
				if ( m_luaTypeConversionFrom[i].typeInfo == _variable->GetTypeInfo() )
				{
					m_luaTypeConversionFrom[i].luaCallback(L,index,_variable);
				}
			}
		}

		void StackDump( lua_State* L, const char* _stateTag = "", const char* _header = "" )
		{
			Logger* _log = static_cast<Logger*>(Engine::Get()->GetSystem("Logger"));

			int _top = lua_gettop(L);

			_log->Log(DROID_LOG_INFO, _stateTag, "Lua Stack dump '%s'", _header);
			_log->Log(DROID_LOG_INFO, _stateTag, "  Stack size : %i", _top);

			for(int i = 1; i <= _top; ++i)
			{
				int type = lua_type(L,i);
				switch (type)
				{
					case LUA_TSTRING:   _log->Log(DROID_LOG_INFO, _stateTag,"  %d: %s\n", i , lua_tostring(  L, i    ) ); break;
					case LUA_TBOOLEAN:  _log->Log(DROID_LOG_INFO, _stateTag,"  %d: %d\n", i , lua_toboolean( L, i    ) ); break;
					case LUA_TNUMBER:   _log->Log(DROID_LOG_INFO, _stateTag,"  %d: %g\n", i , lua_tonumber(  L, i    ) ); break;
					default:            _log->Log(DROID_LOG_INFO, _stateTag,"  %d: %s\n", i , lua_typename(  L, type ) ); break;
				}
			}
		}

		void RegisterLuaFunction( const char* _functionName, Function* _function )
		{
			lua_getglobal(m_state, m_libName); // push table +1
			lua_pushstring( m_state, _functionName ); // push fname +1
			lua_pushlightuserdata( m_state, _function ); // push function pointer +1
			lua_pushcclosure( m_state, ScriptSystemImpl::GenericLuaFunc, 1 ); // push c function +1
			lua_settable(m_state, -3); // set t[k] = v -2
			lua_pop(m_state, 1); // pop table -1
		}

		void RegisterLuaTypeConverter( TypeInfo* _typeInfo, LuaCallback _luaCallback, ConversionType _conversionType )
		{
			LuaTypeConversionPair _pair;
			_pair.typeInfo    = _typeInfo;
			_pair.luaCallback = _luaCallback;

			if ( _conversionType == FROM )
			{
				m_luaTypeConversionFrom.Push(_pair);
			}
		}

		inline lua_State* GetState()
		{
			return m_state;
		}

	private:
		const char*    m_libName;
		lua_State*     m_state;
		Array<Script*> m_scripts;

		typedef struct 
		{
			TypeInfo* typeInfo;
			LuaCallback luaCallback;
		} LuaTypeConversionPair;

		Array<LuaTypeConversionPair> m_luaTypeConversionFrom;
	};
}

using namespace Droid;

ScriptSystem* ScriptSystem::Create()
{
	return new ScriptSystemImpl();
}
