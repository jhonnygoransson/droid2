#include <assert.h>

#include "lua.hpp"

#include "droid_engine.h"
#include "droid_script.h"
#include "droid_logger.h"
#include "droid_script_system.h"

#include "file_system/droid_file.h"


#define DROID_MODULE_TAG "Script"

using namespace Droid;

class ScriptImpl : public Script
{
public:
	ScriptImpl() 
	: m_buffer(0) 
	, m_scriptLength(0)
	, m_state(0)
	, m_isValid(false)
	{}

	~ScriptImpl() 
	{
		Unload();
	}

	void Load(ScriptSystem* _scriptSystem, File& _file)
	{
		// reset state first if we already have data
		if ( m_buffer ) Unload();

		assert(_scriptSystem);

		Logger* _logger  = (Logger*) Engine::Get()->GetSystem("Logger");
		Path _scriptPath = _file.GetPath();

		if ( _file.Open( _scriptPath, Droid::FileSystem::READ ) )
		{
			_logger->Log( DROID_LOG_INFO,DROID_MODULE_TAG,"Loading Script '%s'", _scriptPath.Get());

			DROID_FREE_PTR(m_buffer);

			size_t _sizeDisk = sizeof(char) * _file.Size();
			char* _bytesDisk = (char*) malloc( _sizeDisk + 1 );

			_file.Read( _bytesDisk );
			_file.Close();

			// null-terminate string
			_bytesDisk[ _sizeDisk ] = 0;

			// transfer ownership of buffer
			m_buffer = _bytesDisk;
			m_scriptLength = _sizeDisk;
		} else {
			_logger->Log( DROID_LOG_ERR,DROID_MODULE_TAG,"Loading Script '%s' FAILED", _scriptPath.Get());
			return;
		}

		// create new state from shared global
		m_state       = lua_newthread( _scriptSystem->GetState() );

		// store reference to state registry so it doesn't get GC'd
		m_environment = luaL_ref( m_state, LUA_REGISTRYINDEX); 

		lua_newtable(m_state); //new globals table
        lua_newtable(m_state); //metatable

        lua_pushliteral(m_state, "__index");
        lua_pushvalue(m_state, LUA_GLOBALSINDEX); //original globals 
        lua_settable(m_state, -3);
        lua_setmetatable(m_state, -2);
        lua_replace(m_state, LUA_GLOBALSINDEX); //replace m_state's globals

        lua_rawgeti(m_state, LUA_REGISTRYINDEX, m_environment);

		if ( luaL_loadbuffer(m_state, m_buffer, m_scriptLength,_scriptPath.Get()) != 0)
		{
			_logger->Log( DROID_LOG_ERR,DROID_MODULE_TAG,
				"Running Script '%s' FAILED - Reason : %s", 
				_scriptPath.Get(), lua_tostring(m_state, -1));

          	lua_pop(m_state, 1);  /* pop error message from the stack */
          	return;
		}

		/*
		_scriptSystem->StackDump( m_state, "", "Script - Post luaL_loadbuffer");

		lua_pushvalue(m_state, -2);

		_scriptSystem->StackDump( m_state, "", "Script - Push -2");

		lua_setupvalue(m_state, -2, 1); // function's environment

		_scriptSystem->StackDump( m_state, "", "Script - Post settings upvalue");
		*/

		if ( lua_pcall(m_state, 0, LUA_MULTRET, 0) != 0 )
		{
			_logger->Log( DROID_LOG_ERR,DROID_MODULE_TAG,
				"Running Script '%s' FAILED - Reason : %s", 
				_scriptPath.Get(), lua_tostring(m_state, -1));

          	lua_pop(m_state, 1);  /* pop error message from the stack */
		}

		lua_pop(m_state,1);
		
		m_isValid = true;
	}

	void Unload()
	{
		DROID_FREE_PTR(m_buffer);
		lua_close(m_state);

		m_state        = 0;
		m_scriptLength = 0;
		m_isValid      = false;
	}

	bool IsValid()
	{
		return m_isValid;
	}

private:
	char*      m_buffer;
	size_t     m_scriptLength;
	bool       m_isValid;
	lua_State* m_state;
	int        m_environment;
};

Script* Script::Create()
{
	return new ScriptImpl();
}

