#pragma once

namespace Droid
{
	class File;
	class ScriptSystem;

	class Script
	{
	public:
		Script() {}
		virtual ~Script() {}

		virtual void Load( ScriptSystem* _scriptSystem, File& _file) = 0;
		virtual void Unload() = 0;
		virtual bool IsValid() = 0;

		static Script* Create();
	};
}