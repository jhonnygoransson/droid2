namespace Droid
{
	void GraphicsSystemImpl::TransferPlatformContextToBgfx(const PlatformContext& ctx)
	{
		bgfx::PlatformData pd;
		memset(&pd, 0, sizeof(pd) );
		pd.nwh = ctx.GetWindowHandle();
		bgfx::setPlatformData(pd);
	}
}