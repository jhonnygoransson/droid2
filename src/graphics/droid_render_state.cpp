#include "droid_render_state.h"
#include <bgfx/bgfx.h>

namespace Droid
{
	namespace RenderState
	{
		static const unsigned long long AlphaWrite      =  BGFX_STATE_ALPHA_WRITE;
		static const unsigned long long DepthWrite      =  BGFX_STATE_DEPTH_WRITE;
		static const unsigned long long DepthTestLess  = BGFX_STATE_DEPTH_TEST_LESS;
		static const unsigned long long DepthTestLequal = BGFX_STATE_DEPTH_TEST_LEQUAL;
		static const unsigned long long DepthTestEqual =  BGFX_STATE_DEPTH_TEST_EQUAL;
		static const unsigned long long DepthTestGreaterOrEqual = BGFX_STATE_DEPTH_TEST_GEQUAL;
		static const unsigned long long DepthTestGreater = BGFX_STATE_DEPTH_TEST_GREATER;
		static const unsigned long long DepthTestNotEqual = BGFX_STATE_DEPTH_TEST_NOTEQUAL;
		static const unsigned long long DepthTestNever = BGFX_STATE_DEPTH_TEST_NEVER;
		static const unsigned long long DepthTestAlways =  BGFX_STATE_DEPTH_TEST_ALWAYS;
		static const unsigned long long DepthTestShift =  BGFX_STATE_DEPTH_TEST_SHIFT;
		static const unsigned long long DepthTestMask =  BGFX_STATE_DEPTH_TEST_MASK;
		static const unsigned long long BlendZero = BGFX_STATE_BLEND_ZERO;
		static const unsigned long long BlendOne = BGFX_STATE_BLEND_ONE;
		static const unsigned long long BlendSrcColor = BGFX_STATE_BLEND_SRC_COLOR;
		static const unsigned long long BlendInvSrcColor = BGFX_STATE_BLEND_INV_SRC_COLOR;
		static const unsigned long long BlendSrcAlpha = BGFX_STATE_BLEND_SRC_ALPHA;
		static const unsigned long long BlendInvSrcAlpha = BGFX_STATE_BLEND_INV_SRC_ALPHA;
		static const unsigned long long BlendDstAlpha = BGFX_STATE_BLEND_DST_ALPHA;
		static const unsigned long long BlendInvDstAlpha = BGFX_STATE_BLEND_INV_DST_ALPHA;
		static const unsigned long long BlendDstColor = BGFX_STATE_BLEND_DST_COLOR;
		static const unsigned long long BlendInvDstColor = BGFX_STATE_BLEND_INV_DST_COLOR;
		static const unsigned long long BlendSrcAlhaSat = BGFX_STATE_BLEND_SRC_ALPHA_SAT;
		static const unsigned long long BlendFactor = BGFX_STATE_BLEND_FACTOR;
		static const unsigned long long BlendInvFactor = BGFX_STATE_BLEND_INV_FACTOR;
		static const unsigned long long BlendShift = BGFX_STATE_BLEND_SHIFT;
		static const unsigned long long BlendMask = BGFX_STATE_BLEND_MASK;
		static const unsigned long long BlendEquationAdd = BGFX_STATE_BLEND_EQUATION_ADD;
		static const unsigned long long BlendEquationSub = BGFX_STATE_BLEND_EQUATION_SUB;
		static const unsigned long long BlendEquationRevSub = BGFX_STATE_BLEND_EQUATION_REVSUB;
		static const unsigned long long BlendEquationMin = BGFX_STATE_BLEND_EQUATION_MIN;
		static const unsigned long long BlendEquationMax = BGFX_STATE_BLEND_EQUATION_MAX;
		static const unsigned long long BlendEquationShift = BGFX_STATE_BLEND_EQUATION_SHIFT;
		static const unsigned long long BlendEquationMask = BGFX_STATE_BLEND_EQUATION_MASK;
		static const unsigned long long BlendIndependent = BGFX_STATE_BLEND_INDEPENDENT;
		static const unsigned long long BlendAlphaToCoverage = BGFX_STATE_BLEND_ALPHA_TO_COVERAGE;
		static const unsigned long long CullCW = BGFX_STATE_CULL_CW;
		static const unsigned long long CullCCW = BGFX_STATE_CULL_CCW;
		static const unsigned long long CullShift = BGFX_STATE_CULL_SHIFT;
		static const unsigned long long CullMask = BGFX_STATE_CULL_MASK;
		static const unsigned long long AlphaRefShift = BGFX_STATE_ALPHA_REF_SHIFT;
		static const unsigned long long AlphaRefMask = BGFX_STATE_ALPHA_REF_MASK;
		static const unsigned long long PTTriangleStrip = BGFX_STATE_PT_TRISTRIP;
		static const unsigned long long PTLines = BGFX_STATE_PT_LINES;
		static const unsigned long long PTLineStrip = BGFX_STATE_PT_LINESTRIP;
		static const unsigned long long PTPoints = BGFX_STATE_PT_POINTS;
		static const unsigned long long PTShift = BGFX_STATE_PT_SHIFT;
		static const unsigned long long PTMask = BGFX_STATE_PT_MASK;
		static const unsigned long long PointSizeShift = BGFX_STATE_POINT_SIZE_SHIFT;
		static const unsigned long long PointSizeMask = BGFX_STATE_POINT_SIZE_MASK;
		static const unsigned long long MSAA = BGFX_STATE_MSAA;
		static const unsigned long long LineAA = BGFX_STATE_LINEAA;
		static const unsigned long long ConseravtiveRaster = BGFX_STATE_CONSERVATIVE_RASTER;
		static const unsigned long long ReservedShift = BGFX_STATE_RESERVED_SHIFT;
		static const unsigned long long ReservedMask = BGFX_STATE_RESERVED_MASK;
		static const unsigned long long None = BGFX_STATE_NONE;
		static const unsigned long long Mask = BGFX_STATE_MASK;
		static const unsigned long long Default = BGFX_STATE_DEFAULT;
	}
}