#include "droid_viewport.h"
#include <bgfx/bgfx.h>

namespace Droid
{
	class ViewportImpl : public Viewport
	{ 
	public:
		ViewportImpl(const unsigned int id)
			: clearMask(BGFX_CLEAR_COLOR|BGFX_CLEAR_DEPTH)
			, clearColor(0x303030ff)
			, id(id)
			, vpRenderer(NULL)
		{}

		WindowRect GetRect()
		{
			return windowRect;
		}

		unsigned short GetClearMask()
		{
			return clearMask;
		}

		unsigned int GetClearColor()
		{
			return clearColor;
		}

		void SetRect(WindowRect rect)
		{
			windowRect = rect;
		}

		void SetClearMask(unsigned short clearMask)
		{
			clearMask = clearMask;
		}

		void SetClearColor(unsigned int clearColor)
		{
			clearColor = clearColor;
		}

		void SetViewportRenderer(ViewportRenderer* vpRenderer)
		{
			this->vpRenderer = vpRenderer;
		}

		void Render(double dt)
		{
			if(vpRenderer) vpRenderer->Render(this,dt);
		}

		const unsigned int GetId()
		{
			return id;
		}
		
		ViewportRenderer*  vpRenderer;
		WindowRect         windowRect;
		unsigned short     clearMask;
		unsigned int       clearColor;
		const unsigned int id;
	};
}

namespace Droid
{
	Viewport* Viewport::Create(const unsigned int id)
	{
		return new ViewportImpl(id);
	}
}