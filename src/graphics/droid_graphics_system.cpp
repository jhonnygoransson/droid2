#include <bgfx/bgfx.h>
#include <bgfx/platform.h>

#include "core/droid_configurator.h"
#include "core/droid_engine.h"
#include "droid_graphics_system.h"
#include "droid_engine_events.h"
#include "droid_viewport.h"

#define DEFAULT_WINDOW_W 512
#define DEFAULT_WINDOW_H 512

namespace Droid
{
	struct ShaderHandleWrapper : public ShaderHandle
	{
		bgfx::ShaderHandle handle;
	};

	struct ProgramHandleWrapper : public ProgramHandle
	{
		bgfx::ProgramHandle handle;
	};

	struct VertexBufferHandleWrapper : public VertexBufferHandle
	{
		bgfx::VertexBufferHandle handle; 
	};

	struct IndexBufferHandleWrapper : public IndexBufferHandle
	{
		bgfx::IndexBufferHandle handle;
	};

	struct TextureHandleWrapper : public TextureHandle
	{
		bgfx::TextureHandle handle;
	};

	struct UniformHandleWrapper : public UniformHandle
	{
		bgfx::UniformHandle handle;
	};

	class GraphicsSystemImpl : public GraphicsSystem
	{
	public:
		GraphicsSystemImpl() 
		{
			SubscribeTo("Attached");
			SubscribeTo("RegisterScriptAPI");
		}

		~GraphicsSystemImpl() {}

		void TransferPlatformContextToBgfx(const PlatformContext& ctx);
		
		void InitializeGraphics()
		{
			const PlatformContext& ctx = Engine::Get()->GetPlatformContext();

			TransferPlatformContextToBgfx(ctx);

			Droid::Configurator* _config = static_cast<Droid::Configurator*>(Engine::Get()->GetSystem("Configurator"));
			const char* rendererType     = _config->GetConfigAsString("RendererType", "OpenGL");
			bool fullScreen              = _config->GetConfigAsBool("FullScreen",false);
			int windowWidth              = _config->GetConfigAsInt("WindowWidth",DEFAULT_WINDOW_W);
			int windowHeight             = _config->GetConfigAsInt("WindowHeight",DEFAULT_WINDOW_H);

			ctx.SetWindowSize(windowWidth,windowHeight);

			windowRect.x = 0;
			windowRect.y = 0;
			windowRect.w = windowWidth;
			windowRect.h = windowHeight;

			bgfx::RendererType::Enum bgfxRendererType;

			#define IS_STR_EQUAL(s1,s2) strcmp(s1,s2) == 0

			if (IS_STR_EQUAL(rendererType, "noop"))
				bgfxRendererType = bgfx::RendererType::Noop;
			else if (IS_STR_EQUAL(rendererType, "OpenGL"))
				bgfxRendererType = bgfx::RendererType::OpenGL;
			else if (IS_STR_EQUAL(rendererType, "OpenGLES"))
				bgfxRendererType = bgfx::RendererType::OpenGLES;
			else if (IS_STR_EQUAL(rendererType, "Vulkan"))
				bgfxRendererType = bgfx::RendererType::Vulkan;
			else if (IS_STR_EQUAL(rendererType, "Direct3D9"))
				bgfxRendererType = bgfx::RendererType::Direct3D9;
			else if (IS_STR_EQUAL(rendererType, "Direct3D11"))
				bgfxRendererType = bgfx::RendererType::Direct3D11;
			else if (IS_STR_EQUAL(rendererType, "Metal"))
				bgfxRendererType = bgfx::RendererType::Metal;

			#undef IS_STR_EQUAL

			bgfx::init( bgfxRendererType, BGFX_PCI_ID_NONE );

			// todo: reset types
			// https://bkaradzic.github.io/bgfx/bgfx.html
			bgfx::reset( windowWidth, windowHeight, BGFX_RESET_VSYNC);

			// Set view 0 clear state.
			bgfx::setViewClear(0
				, BGFX_CLEAR_COLOR|BGFX_CLEAR_DEPTH
				, 0x303030ff
				, 1.0f
				, 0
			);
		}

		bool OnEvent(const char* _event)
		{
			if (strcmp(_event,"Attached") == 0)
			{
				InitializeGraphics();
			}

			/*
			if ( strcmp(_event,"RegisterScriptAPI") == 0 )
			{
				RegisterScriptAPI();
			}
			else if ( strcmp(_event,"EngineCreated") == 0 )
			{
				ReadConfig();
			}
			*/
  
			return true;
		}

		void OnEvent(const Droid::Event evt)
		{
			if (evt == WindowEvents::Resize)
			{
				Droid::Configurator* _config = static_cast<Droid::Configurator*>(Engine::Get()->GetSystem("Configurator"));
				windowRect.w                 = _config->GetConfigAsInt("WindowWidth",DEFAULT_WINDOW_W);
				windowRect.h                 = _config->GetConfigAsInt("WindowHeight",DEFAULT_WINDOW_H);

				bgfx::reset( windowRect.w, windowRect.h, BGFX_RESET_VSYNC);
			}
		}

		Viewport* CreateViewport()
		{
			Viewport* viewport = Viewport::Create(viewports.Size());
			viewports.Push(viewport);
			return viewport;
		}

		UniformHandle* CreateUniform(const char* name, UniformType::E uniformType)
		{
			bgfx::UniformType::Enum bgfxUniformType;

			switch (uniformType)
			{
				case(UniformType::Int1):
					bgfxUniformType = bgfx::UniformType::Int1;
					break;
				case(UniformType::Mat3): 
					bgfxUniformType = bgfx::UniformType::Mat3;
					break;
				case(UniformType::Mat4):
					bgfxUniformType = bgfx::UniformType::Mat4;
					break;
				case(UniformType::Vec4):
					bgfxUniformType = bgfx::UniformType::Vec4;
					break;
				default:
					assert(0);
			}

		    bgfx::UniformHandle bgfxHandle = bgfx::createUniform(name, bgfxUniformType);

			if (!bgfx::isValid(bgfxHandle))
			{
				return 0;
			}

			UniformHandleWrapper* handleWrapper = new UniformHandleWrapper;
			handleWrapper->handle = bgfxHandle;
			return handleWrapper;
		}

		TextureHandle* UpdateTexture(TextureHandle* textureHandle, File& fromFile, TextureDescriptor* tDescriptor)
		{
			assert(fromFile.GetMode() & FileSystem::READ);
			assert(fromFile.Size() > 0);
			assert(tDescriptor);

			const size_t fSize = fromFile.Size();
			const size_t srcSize = fSize + 1;

			char* dataCopy = new char[srcSize];
			dataCopy[fSize] = 0x0;

			fromFile.Rewind();
			fromFile.Read(dataCopy);

			const bgfx::Memory* bgfxMem = bgfx::copy((void*)dataCopy,srcSize);

			bgfx::updateTexture2D(
				static_cast<TextureHandleWrapper*>(textureHandle)->handle,
				1, 0,  // TODO!!
				tDescriptor->GetX(), 
				tDescriptor->GetY(), 
				tDescriptor->GetWidth(),
				tDescriptor->GetHeight(), bgfxMem);

			DROID_DEL_ARR(dataCopy);

			return textureHandle;
		}

		TextureHandle* CreateTexture(TextureDescriptor* tDescriptor)
		{
			FileMemory tmpFile;
			tmpFile.Open(Droid::FileSystem::READ);

			TextureHandle* handle = CreateTexture(tmpFile,tDescriptor);
			tmpFile.Close();

			return handle;
		}

		TextureHandle* CreateTexture(File& fromFile, TextureDescriptor* tDescriptor)
		{
			assert(fromFile.GetMode() & FileSystem::READ);

			bgfx::TextureHandle bgfxHandle = bgfx::createTexture2D(
					  tDescriptor->GetWidth()
					, tDescriptor->GetHeight()
					, false
					, 1
					, bgfx::TextureFormat::RGBA8 // TODO: Less hardcodes please
					, BGFX_TEXTURE_NONE);


			if (!bgfx::isValid(bgfxHandle))
			{
				return 0;
			}

			TextureHandleWrapper* handleWrapper = new TextureHandleWrapper;
			handleWrapper->handle = bgfxHandle;

			if (fromFile.Size() > 0)
			{
				UpdateTexture( handleWrapper, fromFile, tDescriptor );
			}

			return handleWrapper;
		}

		ShaderHandle* CreateShader(File& fromFile)
		{
			assert(fromFile.GetMode() & FileSystem::READ);
			assert(fromFile.Size() > 0);
	
			const size_t fSize = fromFile.Size();
			const size_t srcSize = fSize + 1;

			char* dataCopy = new char[srcSize];
			dataCopy[fSize] = 0x0;

			fromFile.Rewind();
			fromFile.Read(dataCopy);

			const bgfx::Memory* bgfxMem   = bgfx::copy((void*)dataCopy,srcSize);
			bgfx::ShaderHandle bgfxHandle = bgfx::createShader(bgfxMem);
			DROID_DEL_ARR(dataCopy);

			if (!bgfx::isValid(bgfxHandle))
			{
				return 0;
			}

			ShaderHandleWrapper* handleWrapper = new ShaderHandleWrapper;
			handleWrapper->handle = bgfxHandle;
			return handleWrapper;
		}

		ProgramHandle* CreateProgram(ShaderHandle* vertexShader, ShaderHandle* fragmentShader)
		{
			ShaderHandleWrapper* vsWrapper = static_cast<ShaderHandleWrapper*>(vertexShader);
			ShaderHandleWrapper* fsWrapper = static_cast<ShaderHandleWrapper*>(fragmentShader);
			assert(vsWrapper && fsWrapper);

			if (!bgfx::isValid(vsWrapper->handle) 
			||  !bgfx::isValid(fsWrapper->handle))
			{
				return 0;
			}

			bgfx::ProgramHandle programHandle = bgfx::createProgram(vsWrapper->handle, fsWrapper->handle, true);

			if (!bgfx::isValid(programHandle))
			{
				return 0;
			}

			ProgramHandleWrapper* handleWrapper = new ProgramHandleWrapper;
			handleWrapper->handle = programHandle;

			return handleWrapper;
		}

		VertexBufferHandle* CreateVertexBuffer(VertexDeclaration* vDecl, File& fromFile)
		{
			assert(fromFile.GetMode() & FileSystem::READ);
			assert(fromFile.Size() > 0);

			char* dataCopy = new char[fromFile.Size()];

			fromFile.Rewind();
			fromFile.Read(dataCopy);

			const bgfx::Memory* bgfxMem = bgfx::copy((void*)dataCopy,fromFile.Size());

			DROID_DEL_ARR(dataCopy);

			const bgfx::VertexDecl* bgfxVertexDecl = static_cast<const bgfx::VertexDecl*>(vDecl->GetInternalVertexDeclaration());

			// TODO: flags
			bgfx::VertexBufferHandle vertexBufferHandle = bgfx::createVertexBuffer(bgfxMem, *bgfxVertexDecl, BGFX_BUFFER_NONE);

			if (!bgfx::isValid(vertexBufferHandle))
			{
				return 0;
			}
 	
 			VertexBufferHandleWrapper* handleWrapper = new VertexBufferHandleWrapper;
			handleWrapper->handle = vertexBufferHandle;

			return handleWrapper;
		}

		IndexBufferHandle* CreateIndexBuffer(File& fromFile)
		{
			assert(fromFile.GetMode() & FileSystem::READ);
			assert(fromFile.Size() > 0);

			char* dataCopy = new char[fromFile.Size()];

			fromFile.Rewind();
			fromFile.Read(dataCopy);

			const bgfx::Memory* bgfxMem = bgfx::copy((void*)dataCopy,fromFile.Size());

			DROID_DEL_ARR(dataCopy);

			// todo: flags
			bgfx::IndexBufferHandle indexBufferHandle = bgfx::createIndexBuffer(bgfxMem, BGFX_BUFFER_NONE);

			if (!bgfx::isValid(indexBufferHandle))
			{
				return 0;
			}

			IndexBufferHandleWrapper* handleWrapper = new IndexBufferHandleWrapper;
			handleWrapper->handle = indexBufferHandle;

			return handleWrapper;
		}

		void SetViewTransform(int viewport, Mat4x4 viewTransform, Mat4x4 projectionTransform)
		{
			float* viewTransformAsArray       = static_cast<float*>(viewTransform[0]);
			float* projectionTransformAsArray = static_cast<float*>(projectionTransform[0]);

			bgfx::setViewTransform(viewport, viewTransformAsArray, projectionTransformAsArray);
		}

		void SetModelTransform(Mat4x4 modelTransform)
		{
			float* modelTransformAsArray = static_cast<float*>(modelTransform[0]);
			bgfx::setTransform(modelTransformAsArray);
		}

		void SetVertexBuffer(VertexBufferHandle* vertexBufferHandle)
		{
			VertexBufferHandleWrapper* wrapper = static_cast<VertexBufferHandleWrapper*>(vertexBufferHandle);
			bgfx::setVertexBuffer(0,wrapper->handle); // not sure about stream index
		}

		void SetIndexBuffer(IndexBufferHandle* indexBufferHandle)
		{
			IndexBufferHandleWrapper* wrapper = static_cast<IndexBufferHandleWrapper*>(indexBufferHandle);
			bgfx::setIndexBuffer(wrapper->handle);
		}

		void Submit(int viewport, ProgramHandle* programHandle, int depth, bool preserveState)
		{
			ProgramHandleWrapper* wrapper = static_cast<ProgramHandleWrapper*>(programHandle);
			bgfx::submit(viewport,wrapper->handle,depth,preserveState);
		}

		void SetState(unsigned long long state, unsigned int rgba)
		{
			bgfx::setState(state,rgba);
		}

		void SetTexture(unsigned unit, UniformHandle* uniformHandle, TextureHandle* textureHandle)
		{
			assert(uniformHandle);
			assert(textureHandle);

			bgfx::setTexture(unit, static_cast<UniformHandleWrapper*>(uniformHandle)->handle, static_cast<TextureHandleWrapper*>(textureHandle)->handle);
		}

		void Update(double _dt)
		{
			Publisher::Notify(GraphicsEvents::Render);

			for( unsigned int i=0; i < viewports.Size(); i++ )
			{
				Viewport*  viewport     = viewports[i];
				WindowRect viewportRect = viewport->GetRect();

				bgfx::setViewRect(
					i, 
					viewportRect.x, 
					viewportRect.y, 
					uint16_t(viewportRect.w), 
					uint16_t(viewportRect.h));

				bgfx::setViewClear(
					i,
					viewport->GetClearMask(),
					viewport->GetClearColor(),
					1.0f,
					0
				);

				viewport->Render(_dt);
			}

			bgfx::touch(0);
			bgfx::frame();

			Publisher::Notify(GraphicsEvents::PostRender);
		}

		WindowRect       windowRect;
		Array<Viewport*> viewports;
	};
}

#include "droid_graphics_system_platform_impl.inl"

using namespace Droid;

GraphicsSystem* GraphicsSystem::Create()
{
	return new GraphicsSystemImpl();
}