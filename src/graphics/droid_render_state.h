#pragma once

namespace Droid
{
	namespace RenderState
	{
		extern const unsigned long long AlphaWrite;
		extern const unsigned long long DepthWrite;
		extern const unsigned long long DepthTestLess;
		extern const unsigned long long DepthTestLequal;
		extern const unsigned long long DepthTestEqual;
		extern const unsigned long long DepthTestGreaterOrEqual;
		extern const unsigned long long DepthTestGreater;
		extern const unsigned long long DepthTestNotEqual;
		extern const unsigned long long DepthTestNever;
		extern const unsigned long long DepthTestAlways;
		extern const unsigned long long DepthTestShift;
		extern const unsigned long long DepthTestMask;
		extern const unsigned long long BlendZero;
		extern const unsigned long long BlendOne;
		extern const unsigned long long BlendSrcColor;
		extern const unsigned long long BlendInvSrcColor;
		extern const unsigned long long BlendSrcAlpha;
		extern const unsigned long long BlendInvSrcAlpha;
		extern const unsigned long long BlendDstAlpha;
		extern const unsigned long long BlendInvDstAlpha;
		extern const unsigned long long BlendDstColor;
		extern const unsigned long long BlendInvDstColor;
		extern const unsigned long long BlendSrcAlhaSat;
		extern const unsigned long long BlendFactor;
		extern const unsigned long long BlendInvFactor;
		extern const unsigned long long BlendShift;
		extern const unsigned long long BlendMask;
		extern const unsigned long long BlendEquationAdd;
		extern const unsigned long long BlendEquationSub;
		extern const unsigned long long BlendEquationRevSub;
		extern const unsigned long long BlendEquationMin;
		extern const unsigned long long BlendEquationMax;
		extern const unsigned long long BlendEquationShift;
		extern const unsigned long long BlendEquationMask;
		extern const unsigned long long BlendIndependent;
		extern const unsigned long long BlendAlphaToCoverage;
		extern const unsigned long long CullCW;
		extern const unsigned long long CullCCW;
		extern const unsigned long long CullShift;
		extern const unsigned long long CullMask;
		extern const unsigned long long AlphaRefShift;
		extern const unsigned long long AlphaRefMask;
		extern const unsigned long long PTTriangleStrip;
		extern const unsigned long long PTLines;
		extern const unsigned long long PTLineStrip;
		extern const unsigned long long PTPoints;
		extern const unsigned long long PTShift;
		extern const unsigned long long PTMask;
		extern const unsigned long long PointSizeShift;
		extern const unsigned long long PointSizeMask;
		extern const unsigned long long MSAA;
		extern const unsigned long long LineAA;
		extern const unsigned long long ConseravtiveRaster;
		extern const unsigned long long ReservedShift;
		extern const unsigned long long ReservedMask;
		extern const unsigned long long None;
		extern const unsigned long long Mask;
		extern const unsigned long long Default;

		/*
		TODO:
		AlphaRef
		PointSize
		BlendFuncSeparate
		BlendEquationSeparate
		BlendFunc
		BlendEquation
		BlendAdd
		BlendAlpha
		BlendDarken
		BlendLighten
		BlendMultiply
		BlendNormal
		BlendScreen
		BlendLinearBurn
		BlendFuncRTX
		BlendFuncRTXE
		BlendFuncRT1
		BlendFuncRT2
		BlendFuncRT3
		BlendFuncRT1E
		BlendFuncRT2E
		BlendFuncRT3E
		*/
	};
}