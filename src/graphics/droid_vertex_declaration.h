#pragma once

namespace Droid
{
	struct Attribute
	{
		enum E 
		{ 
			Position,
			Normal,
			Tangent,
			Bitangent,
			Color0,
			Color1,
			Indices,
			Weight,
			TexCoord0,
			TexCoord1,
			TexCoord2,
			TexCoord3,
			TexCoord4,
			TexCoord5,
			TexCoord6,
			TexCoord7
		};
	};


	struct AttributeType
	{
		enum E
		{
			UInt8,
			UInt10,
			Int16,
			Half,
			Float
		};
	};

	class VertexDeclaration
	{
	public:
		virtual void AddAttribute( Attribute::E attribute, unsigned int componentSize, AttributeType::E attributeType, bool asInt = false ) = 0;
		virtual void Commit() = 0;
		static VertexDeclaration* Create();
	private:
		friend class GraphicsSystemImpl;
		virtual void* GetInternalVertexDeclaration() = 0;
	};
}