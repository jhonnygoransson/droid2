#pragma once

namespace Droid
{
	struct UniformType
	{
		enum E
		{
			Int1,
			Mat3,
			Mat4,
			Vec4
		};
	};

	struct GraphicsHandle
	{
		GraphicsHandle(): id(0){}
		int id; // unused
	};

	struct ShaderHandle       : public GraphicsHandle {};
	struct ProgramHandle      : public GraphicsHandle {};
	struct VertexBufferHandle : public GraphicsHandle {};
	struct IndexBufferHandle  : public GraphicsHandle {};
	struct TextureHandle      : public GraphicsHandle {};
	struct UniformHandle      : public GraphicsHandle {};
}