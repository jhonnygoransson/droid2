#pragma once

#include "core/droid_common.h"

namespace Droid
{
	/*
	struct Format
	{
		enum E
		{
			
		};
	};
	*/

	struct ImageRect
	{
		unsigned x;
		unsigned y;
		unsigned width;
		unsigned height;
	};

	class TextureDescriptor
	{
	public:
		TextureDescriptor()
		: m_imageRect({0,0,0,0})
		{}

		TextureDescriptor(ImageRect rect)
		: m_imageRect(rect)
		{}

		unsigned GetWidth()	
		{
			return m_imageRect.width;
		}

		unsigned GetHeight() 
		{
			return m_imageRect.height;
		}

		unsigned GetX()
		{
			return m_imageRect.x;
		}

		unsigned GetY()
		{
			return m_imageRect.y;
		}

	private:
		friend class GraphicsSystemImpl;
		const ImageRect m_imageRect;
	};
}