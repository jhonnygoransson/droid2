#pragma once

#include "core/droid_system.h"
#include "core/file_system/droid_file_memory.h"
#include "droid_graphics_interfaces.h"
#include "droid_vertex_declaration.h"
#include "math/droid_math.h"
#include "droid_render_state.h"
#include "droid_texture_descriptor.h"

namespace Droid
{
	class PlatformContext;
	class Viewport;
	class GraphicsSystem : public System
	{
	public:
		GraphicsSystem() : System("GraphicsSystem") {}
		virtual ~GraphicsSystem() {}

		virtual void                TransferPlatformContextToBgfx(const PlatformContext& ctx) = 0;

		virtual ShaderHandle*       CreateShader(File& fromFile) = 0;
		virtual ProgramHandle*      CreateProgram(ShaderHandle* vertexShader, ShaderHandle* fragmentShader) = 0;
		virtual VertexBufferHandle* CreateVertexBuffer(VertexDeclaration* vDecl, File& fromFile) = 0;
		virtual IndexBufferHandle*  CreateIndexBuffer(File& fromFile) = 0;
		virtual TextureHandle*      CreateTexture(File& fromFile, TextureDescriptor* tDescriptor ) = 0;
		virtual TextureHandle*      CreateTexture(TextureDescriptor* tDescriptor) = 0;
		virtual Viewport*           CreateViewport() = 0;
		virtual UniformHandle*      CreateUniform(const char* name, UniformType::E uniformType) = 0;

		virtual TextureHandle*      UpdateTexture(TextureHandle* textureHandle, File& fromFile, TextureDescriptor* tDescriptor = 0) = 0;

		virtual void                SetVertexBuffer(VertexBufferHandle* vertexBufferHandle) = 0;
		virtual void                SetIndexBuffer(IndexBufferHandle* indexBufferHandle) = 0;
		virtual void                SetViewTransform(int viewport, Mat4x4 viewTransform, Mat4x4 projectionTransform) = 0;
		virtual void                SetModelTransform(Mat4x4 modelTransform) = 0;
		virtual void                SetState(unsigned long long state, unsigned int rgba=0 ) = 0;
		virtual void                SetTexture(unsigned unit, UniformHandle* uniformHandle, TextureHandle* textureHandle) = 0;

		virtual void                Submit(int viewport, ProgramHandle* programHandle, int depth=0, bool preserveState=false) = 0;
		
		static GraphicsSystem*      Create();
	};
}