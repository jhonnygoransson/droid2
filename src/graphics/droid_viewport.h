#pragma once

#include "droid_interfaces.h"

namespace Droid
{	
	class Viewport;
	class ViewportRenderer
	{
	public:
		virtual void Render(Viewport* vp, double dt) = 0;
	};

	class Viewport
	{ 
	public:
		friend  class              GraphicsSystemImpl;
		virtual void               SetRect(WindowRect rect) = 0;
		virtual void               SetClearMask(unsigned short clearMask) = 0;
		virtual void               SetClearColor(unsigned int clearColor) = 0;
		virtual WindowRect         GetRect() = 0;
		virtual unsigned short     GetClearMask() = 0;
		virtual unsigned int       GetClearColor() = 0;
		virtual const unsigned int GetId() = 0;
		virtual void               SetViewportRenderer(ViewportRenderer* vpRenderer) = 0;
		virtual void               Render(double dt) = 0;
	private:
		static Viewport*           Create(const unsigned int id);
	};
}