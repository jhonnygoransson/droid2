#include <bgfx/bgfx.h>

#include "droid_vertex_declaration.h"
#include "core/droid_common.h"

namespace Droid
{
	bgfx::Attrib::Enum GetBgfxAttribByEnum(Attribute::E attribute)
	{
		switch(attribute)
		{
			case(Attribute::Position):  return bgfx::Attrib::Position;
			case(Attribute::Normal):    return bgfx::Attrib::Normal;
			case(Attribute::Tangent):   return bgfx::Attrib::Tangent;
			case(Attribute::Bitangent): return bgfx::Attrib::Bitangent;
			case(Attribute::Color0):    return bgfx::Attrib::Color0;
			case(Attribute::Color1):    return bgfx::Attrib::Color1;
			case(Attribute::Indices):   return bgfx::Attrib::Indices;
			case(Attribute::Weight):    return bgfx::Attrib::Weight;
			case(Attribute::TexCoord0): return bgfx::Attrib::TexCoord0;
			case(Attribute::TexCoord1): return bgfx::Attrib::TexCoord1;
			case(Attribute::TexCoord2): return bgfx::Attrib::TexCoord2;
			case(Attribute::TexCoord3): return bgfx::Attrib::TexCoord3;
			case(Attribute::TexCoord4): return bgfx::Attrib::TexCoord4;
			case(Attribute::TexCoord5): return bgfx::Attrib::TexCoord5;
			case(Attribute::TexCoord6): return bgfx::Attrib::TexCoord6;
			case(Attribute::TexCoord7): return bgfx::Attrib::TexCoord7;
		}

		return bgfx::Attrib::Count; // invalid
	}

	bgfx::AttribType::Enum GetBgfxAttribTypeByEnum(AttributeType::E attributeType)
	{
		switch(attributeType)
		{
			case(AttributeType::UInt8):  return bgfx::AttribType::Uint8;
			case(AttributeType::UInt10): return bgfx::AttribType::Uint10;
			case(AttributeType::Int16):  return bgfx::AttribType::Int16;
			case(AttributeType::Half):   return bgfx::AttribType::Half;
			case(AttributeType::Float):  return bgfx::AttribType::Float;
		}

		return bgfx::AttribType::Count;
	}

	typedef struct 
	{
		Attribute::E     attribute;
		AttributeType::E attributeType;
		unsigned int     componentSize;
		bool             asInt;
	} VertexDeclarationEntry;

	class VertexDeclarationImpl : public VertexDeclaration
	{
		void AddAttribute( Attribute::E attribute, unsigned int componentSize, AttributeType::E attributeType, bool asInt )
		{
			VertexDeclarationEntry vDeclEntry;
			vDeclEntry.attribute     = attribute;
			vDeclEntry.attributeType = attributeType;
			vDeclEntry.componentSize = componentSize;
			vDeclEntry.asInt         = asInt;

			m_entries.Push(vDeclEntry);
		}

		void Commit()
		{
			m_bgfxVertexDecl.begin();

			for( unsigned int i=0; i < m_entries.Size(); i++)
			{
				bgfx::Attrib::Enum bgfxAttribName        = GetBgfxAttribByEnum(m_entries[i].attribute);
				bgfx::AttribType::Enum bgfxAttributeType = GetBgfxAttribTypeByEnum(m_entries[i].attributeType);
				unsigned int bgfxComponentSize           = m_entries[i].componentSize;
				bool asInt                               = m_entries[i].asInt;

				m_bgfxVertexDecl.add(bgfxAttribName,bgfxComponentSize,bgfxAttributeType,asInt);
			}

			m_bgfxVertexDecl.end();
		}

		void* GetInternalVertexDeclaration()
		{
			return &m_bgfxVertexDecl;
		}

		bgfx::VertexDecl              m_bgfxVertexDecl;
		Array<VertexDeclarationEntry> m_entries;
	};

	VertexDeclaration* VertexDeclaration::Create()
	{
		return new VertexDeclarationImpl;
	}
}