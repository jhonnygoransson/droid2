#pragma once

#include "core/droid_msg_queue.h"

#define DROID_CONTEXT_DEFAULT_WIDTH  512
#define DROID_CONTEXT_DEFAULT_HEIGHT 512

namespace Droid
{
	struct PlatFormResizeMsg
	{
		unsigned int w;
		unsigned int h;
	};

	class PlatformContext
	{
	public:
		PlatformContext();
		~PlatformContext();
		void* GetWindowHandle() const;

		void SetFullScreen(bool value) const;
		void SetWindowSize(unsigned int width, unsigned int height) const;

		MsgQueue_Msg PollMessage() const;
	};
}