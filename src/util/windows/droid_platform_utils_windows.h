#pragma once

#define DROID_LAST_WINDOWS_ERROR(buf) \
 	char buf[256]; \
	Droid::GetLastWindowsError( buf, 256 ); 

namespace Droid
{
	void GetLastWindowsError( char* _buffer, size_t _buffer_size );
}