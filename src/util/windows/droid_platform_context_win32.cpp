#include "../droid_platform_context.h"
#include "droid_platform_utils_windows.h"
#include "core/droid_msg_queue.h"

#include <windows.h>
#include <Windowsx.h>
#include <Commctrl.h>
#include <process.h>

namespace Droid
{
	class PlatformContextWin32
	{
	public:
		LRESULT CALLBACK subWndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
		{
		    switch (msg)
		    {
				case WM_QUIT:
				case WM_CLOSE:
				{
					m_msgQueue.Push("WINDOW_CLOSE",0);
				} break;
				case WM_DESTROY:
		        case WM_SIZE:
				{
					PlatFormResizeMsg msg;
					msg.w = GET_X_LPARAM(lParam);
					msg.h = GET_Y_LPARAM(lParam);

					m_msgQueue.Push("WINDOW_RESIZE", static_cast<void*>(&msg), sizeof(PlatFormResizeMsg), 0, true);

				} break;
				case WM_SIZING:
				break;
		        case WM_NCDESTROY:
		          RemoveWindowSubclass(hWnd, subWndProcForwarder, uIdSubclass);
		          break;
		    }

		    return DefSubclassProc(hWnd, msg, wParam, lParam);
		}

		UINT CALLBACK wndProcHandler(LPVOID lpParams)
		{
			UNREFERENCED_PARAMETER(lpParams);
			
			volatile bool threadRunning = true;

			while(threadRunning)
			{
				MSG msg = {0};
				DWORD obj = MsgWaitForMultipleObjectsEx(
					0, NULL, INFINITE,FALSE, QS_ALLINPUT);

				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					switch (msg.message)
					{
						case WM_USER_SHUTDOWN:
						{
							threadRunning = false;	
						} break;
						case WM_USER_CREATE_WINDOW:
						{
							HWND* hWnd = (HWND*)msg.lParam;
							LPCREATESTRUCT cp = (LPCREATESTRUCT)msg.wParam;
							
							*hWnd = CreateWindowEx(
										cp->dwExStyle, cp->lpszClass,
										cp->lpszName, cp->style, 
										cp->x, cp->y, cp->cx, cp->cy,
										cp->hwndParent, cp->hMenu, 
										cp->hInstance, NULL);

							SetWindowSubclass(*hWnd, subWndProcForwarder, 0, 0);

							// notify main thread we have finished creating window
							SetEvent(cp->lpCreateParams);
						} break;
						case WM_USER_SET_SIZE:
						{
							HWND* hWnd      = (HWND*)msg.lParam;
							WindowMsg* wMsg = (WindowMsg*)msg.wParam;

							RECT clientRect, windowRect;
						   	GetClientRect(*hWnd, &clientRect);
						   	GetWindowRect(*hWnd, &windowRect);

						   	const long windowWidth  = windowRect.right - windowRect.left;
							const long windowHeight = windowRect.bottom - windowRect.top;
							const long clientWidth  = clientRect.right - clientRect.left;
							const long clientHeight = clientRect.bottom - clientRect.top;
							const long widthDiff    = windowWidth - clientWidth;
							const long heightDiff   = windowHeight - clientHeight;

							RECT newrect = {windowRect.left, windowRect.right, (LONG)wMsg->w+widthDiff, (LONG)wMsg->h+heightDiff};

							AdjustWindowRectEx(
								&newrect, 
								GetWindowStyle(*hWnd), 
								GetMenu(*hWnd) != NULL, 
								GetWindowExStyle(*hWnd));

							UpdateWindow(*hWnd);
							SetWindowPos(*hWnd, HWND_TOP, windowRect.left, windowRect.top, wMsg->w+widthDiff, wMsg->h+heightDiff, SWP_SHOWWINDOW);
							ShowWindow(*hWnd, SW_RESTORE);
							delete wMsg;
						} break;
						default:
						{
							TranslateMessage(&msg);
							DispatchMessage(&msg);	
						} break;
					}
				}
			}

			_endthreadex(0);
			return 0;
		}

		HWND CreateWindowThreaded(DWORD threadId)
		{
			HWND hWnd = NULL;
			CREATESTRUCT cp = {0};
			cp.hInstance = GetModuleHandle(NULL);
			cp.cy = CW_USEDEFAULT;
			cp.cx = CW_USEDEFAULT;
			cp.y = CW_USEDEFAULT;
			cp.x = CW_USEDEFAULT;
			cp.style = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
			cp.lpszName = "Droid";
			cp.lpszClass = "Droid_Class";
			cp.dwExStyle = WS_EX_APPWINDOW;
			cp.lpCreateParams = CreateEvent(
				NULL, TRUE, FALSE, "WM_USER_CREATE_WINDOW");

			while( 0 == PostThreadMessage(
				(DWORD) threadId, WM_USER_CREATE_WINDOW,
				(WPARAM)&cp, (LPARAM)&hWnd))
			{
				Sleep(0);
			}

			if (WaitForSingleObject(cp.lpCreateParams,INFINITE) != WAIT_OBJECT_0)
			{
				DROID_LAST_WINDOWS_ERROR(errbuf);
			}

			CloseHandle(cp.lpCreateParams);

			return hWnd;
		}

		void setWindowSize(unsigned int width,unsigned int height)
		{
			WindowMsg* msg = new WindowMsg;
			msg->w = width;
			msg->h = height;

			PostThreadMessage(
				(DWORD) m_threadId, WM_USER_SET_SIZE,
				(WPARAM)msg, (LPARAM)&m_windowHandle);
		}

		void setFullScreen(bool value)
		{
			// not implemented
		}

		void main()
		{
			HINSTANCE instance = (HINSTANCE)GetModuleHandle(NULL);

			WNDCLASSEXA wnd;
			unsigned int threadId;
			memset(&wnd, 0, sizeof(wnd) );
			wnd.cbSize        = sizeof(wnd);
			wnd.style         = CS_HREDRAW | CS_VREDRAW;
			wnd.lpfnWndProc   = DefWindowProc;
			wnd.hInstance     = instance;
			wnd.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
			wnd.hCursor       = LoadCursor(NULL, IDC_ARROW);
			wnd.lpszClassName = "Droid_Class";
			wnd.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
			RegisterClassExA(&wnd);

			m_uiThreadHandle = (HANDLE)_beginthreadex(
				NULL, 0, wndProcForwarder, 0, 0,&threadId);
   			m_windowHandle   = CreateWindowThreaded(threadId);
   			m_threadId       = threadId;
		}

		MsgQueue_Msg pollMessage()
		{
			return m_msgQueue.PopNonBlocking();
		}

		void shutdown()
		{
			// not implemented
		}

		typedef struct 
		{
			unsigned int x;
			unsigned int y;
			unsigned int w;
			unsigned int h;
		} WindowMsg;

		enum
		{
			WM_USER_CREATE_WINDOW = WM_USER,
			WM_USER_SHUTDOWN,
			WM_USER_SET_SIZE
		};

		static UINT CALLBACK wndProcForwarder(LPVOID lpParams);
		static LRESULT CALLBACK subWndProcForwarder(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData);

		HWND         m_windowHandle;
		HANDLE       m_uiThreadHandle;
		unsigned int m_threadId;
		MsgQueue     m_msgQueue;
	};

	static PlatformContextWin32 s_ctx;

	UINT CALLBACK PlatformContextWin32::wndProcForwarder(LPVOID lpParams)
	{
		return s_ctx.wndProcHandler(lpParams);
	}

	LRESULT CALLBACK PlatformContextWin32::subWndProcForwarder(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
	{
		return s_ctx.subWndProcHandler(hWnd,msg,wParam,lParam,uIdSubclass,dwRefData);
	}

	PlatformContext::PlatformContext()
	{
		s_ctx.main();
	}

	PlatformContext::~PlatformContext()
	{
		s_ctx.shutdown();
	}

	void* PlatformContext::GetWindowHandle() const
	{
		return s_ctx.m_windowHandle;
	}

	void PlatformContext::SetFullScreen(bool value) const
	{
		s_ctx.setFullScreen(value);
	}
	
	void PlatformContext::SetWindowSize(unsigned int width, unsigned int height) const
	{
		s_ctx.setWindowSize(width,height);
	}

	MsgQueue_Msg PlatformContext::PollMessage() const
	{
		return s_ctx.pollMessage();
	}
}