#include <windows.h>
#include <stdio.h>

#include "droid_platform_utils_windows.h"

void Droid::GetLastWindowsError( char* _buffer, size_t _buffer_size )
{	
	DWORD _ret_size;
	LPTSTR _tmp=NULL;

	_ret_size=FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|
                           FORMAT_MESSAGE_FROM_SYSTEM|
                           FORMAT_MESSAGE_ARGUMENT_ARRAY,
                           NULL,
                           GetLastError(),
                           LANG_NEUTRAL,
                           (LPTSTR)&_tmp,
                           0,
                           NULL );

     if (!_ret_size || _tmp == NULL) {
          _buffer[0]='\0';
     }
     else {
          _tmp[strlen(_tmp)-2]='\0'; //remove cr and newline character
          sprintf_s(_buffer,_buffer_size,"%0.*s (0x%x)",_buffer_size-16,_tmp,GetLastError());
          LocalFree((HLOCAL)_tmp);
     }
}