#pragma once

namespace Droid
{
	// data descriptors
	struct TypeInfo
	{
		template <typename T> void PlacementCopy( void* _data, void* _src );
		
		const char* typeName;
		unsigned    size;
	};

	struct GameComponentType
	{	
		GameComponentType()
		: mask(0) {}

		unsigned int mask;
		unsigned int size;
	};

	struct WindowRect
	{
		WindowRect()
		: x(0),y(0),w(0),h(0) {}
		WindowRect(unsigned int x, unsigned int y,unsigned int w, unsigned int h)
		: x(x),y(y),w(w),h(h) {}

		unsigned int x;
		unsigned int y;
		unsigned int w;
		unsigned int h;
	};

	enum EngineState
	{
		UnInitialized = -1,
		Stopped,
		Running,
		Paused
	};
}