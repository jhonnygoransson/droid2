#pragma once

#include "droid_interfaces.h"

#define DROID_ENTITY_FREE 0

namespace Droid
{
	class GameEntity
	{
	public:
		typedef unsigned int GuidType;
		typedef unsigned int MaskType;
	public:
		GameEntity()
		: m_guid(0)
		, m_componentMask(0) {}

		void AddComponent( GameComponentType _type );
		void RemoveComponent( GameComponentType _type );
		bool HasComponent( GameComponentType _type );
		bool IsFree();
		bool IsTaken();

		GuidType GetId() { return m_guid; }

	protected:
		friend class GameComponentSystemImpl;

		GuidType m_guid;
		MaskType m_componentMask;

	public:
		bool operator==(const GameEntity& rhs) { return this == &rhs; }
		bool operator!=(const GameEntity& rhs) { return this != &rhs; }

		static GameEntity& NONE;
	};

	inline void GameEntity::AddComponent( GameComponentType _type )
	{
		m_componentMask = m_componentMask | _type.mask;
	}

	inline void GameEntity::RemoveComponent( GameComponentType _type )
	{
		m_componentMask = m_componentMask & (~_type.mask);
	}

	inline bool GameEntity::HasComponent( GameComponentType _type )
	{
		return (m_componentMask & _type.mask) == _type.mask;
	}

	inline bool GameEntity::IsFree()
	{
		return m_guid == DROID_ENTITY_FREE;
	}

	inline bool GameEntity::IsTaken()
	{
		return m_guid != DROID_ENTITY_FREE;
	}
}