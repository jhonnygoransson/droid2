#include "droid_game_component_system.h"
#include "droid_game_entity.h"
#include "droid_game_component.h"

#include <math.h>

namespace Droid
{
	class GameComponentSystemImpl : public GameComponentSystem
	{
	public:

		GameComponentSystemImpl() {}
		~GameComponentSystemImpl() 
		{
			Reset();
		}

		GameComponentType CreateComponentType()
		{
			GameComponentType _newType;
			_newType.mask = (unsigned int) pow(2.0, (int) m_componentTypes.Size());

			m_componentTypes.Push(_newType);

			return _newType;
		}

		void Reset()
		{
			for( unsigned int i = 0; i < m_components.Size(); i++ )
			{
				DROID_FREE_PTR(m_components[i]);
			}

			m_componentTypes.Reset();
			m_entities.Reset();
			m_components.Reset();
		}

		void AllocateEntities(unsigned int _amount)
		{
			m_entities.Resize(_amount);
		}

		GameEntity& NewEntity()
		{
			for( unsigned int i = 0; i < m_entities.Size(); i++ )
			{
				if ( m_entities[i].IsFree() )
				{
					m_entities[i].m_guid          = i+1;
					m_entities[i].m_componentMask = 0;

					return m_entities[i];
				}
			}

			return GameEntity::NONE;
		}

		GameComponentType NewComponent( unsigned int _componentSize )
		{
			GameComponentType 
			_gt      = CreateComponentType();
			_gt.size = _componentSize;

			GameComponentContainer _newComponentData;
			_newComponentData = malloc( _componentSize * GetCapacity() );
			memset( _newComponentData, 0, _componentSize * GetCapacity() );

			// GameComponent* componentPtr = static_cast<GameComponent*>(_newComponentData);

			char* componentDataPtr = static_cast<char*>(_newComponentData);

			// set component data type for all components
			for(unsigned int i=0; i < GetCapacity(); i++)
			{
				char* componentRaw       = &componentDataPtr[i * _componentSize];
				GameComponent* component = reinterpret_cast<GameComponent*>(componentRaw);
				component->SetGameComponentType(_gt);
			}

			m_components.Push( _newComponentData );

			return _gt;
		}

		void* GetRawComponent( GameComponentType _type, unsigned int _componentIndex ) 
		{
			int _typeToArrayIndex = (int) (log( (double) _type.mask ) / log( 2.0 ));

			assert( static_cast<unsigned int>(_typeToArrayIndex) < m_components.Size());

			unsigned int _componentArrayOffset = _componentIndex * _type.size;
			char* c_componentPtr               = ((char*) m_components[_typeToArrayIndex] + _componentArrayOffset);

			return (void*) c_componentPtr;
		}

		unsigned int GetCapacity()
		{
			return m_entities.Size();
		}

		GameEntity& GetEntity(unsigned int _index)
		{
			if (_index < m_entities.Size())
				return m_entities[_index];
			
			return GameEntity::NONE;
		}

		GameEntity& GetEntityByGuid(unsigned int _guid)
		{
			unsigned int _index = _guid-1;

			if (_index < m_entities.Size())
				return m_entities[_index];

			return GameEntity::NONE;
		}

	private:

		typedef void* GameComponentContainer;

		Array<GameComponentType>      m_componentTypes;
		Array<GameEntity>             m_entities;
		Array<GameComponentContainer> m_components;
	};
}

using namespace Droid;

GameComponentSystem* GameComponentSystem::Create()
{
	return new GameComponentSystemImpl();
}