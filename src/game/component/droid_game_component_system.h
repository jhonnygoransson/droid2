#pragma once

#include "droid_interfaces.h"
#include "core/droid_system.h"
#include "droid_game_entity.h"

namespace Droid
{
	class GameComponentSystem : public System
	{
	public:
		GameComponentSystem() : System("GameComponentSystem") {}
		virtual ~GameComponentSystem() {}

		virtual void                AllocateEntities(unsigned int _amount) = 0;
		virtual GameEntity&         NewEntity() = 0;
		virtual GameComponentType   NewComponent( unsigned int _componentSize ) = 0;
		virtual GameComponentType   CreateComponentType() = 0;
		virtual void*               GetRawComponent( GameComponentType _type, unsigned int _componentIndex ) = 0;
		virtual unsigned int        GetCapacity() = 0;
		virtual GameEntity&         GetEntity(unsigned int _index) = 0;
		virtual GameEntity&         GetEntityByGuid(unsigned int _guid) = 0;

		template <typename T>
		GameComponentType AllocateComponent();

		template <typename T>
		T* GetComponent(unsigned int _index);

		template <typename T>
		T* GetComponent(GameEntity& _entity);

		static GameComponentSystem* Create();
	protected:

		template <typename T>
		GameComponentType* GetGameComponentTypeInstance()
		{
			static GameComponentType _typeInstance;
			return &_typeInstance;
		}
	};	

	template <typename T>
	GameComponentType GameComponentSystem::AllocateComponent()
	{
		GameComponentType _newType     = NewComponent(sizeof(T));
		GameComponentType* _newTypeMap = GetGameComponentTypeInstance<T>();

		_newTypeMap->mask = _newType.mask;
		_newTypeMap->size = _newType.size;

		return _newType;
	}

	template <typename T>
	T* GameComponentSystem::GetComponent(unsigned int _index)
	{
		GameComponentType* _type = GetGameComponentTypeInstance<T>();
		T* componentPtr          = ((T*) GetRawComponent( (*_type), _index ));

		return 
	}
	
	template <typename T>
	T* GameComponentSystem::GetComponent(GameEntity& _entity)
	{
		GameComponentType* _type = GetGameComponentTypeInstance<T>();
		return ((T*) GetRawComponent( (*_type), _entity.GetId()-1 ));
	}
}