#pragma once

#include "droid_interfaces.h"

namespace Droid
{
	class GameComponent
	{
	public:
		GameComponent() {}
		GameComponent( GameComponentType _type )
		: m_type(_type) 
		{}

		virtual ~GameComponent() {}

		inline void SetGameComponentType(GameComponentType _type)
		{
			m_type = _type;
		}

		inline GameComponentType GetGameComponentType()
		{
			return m_type;
		}

	private:
		GameComponentType m_type;
	};	
}